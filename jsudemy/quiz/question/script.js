
function Question(question, answers, correctAnswer){
	this.question = question;
	this.answers = answers;
	this.correctAnswer = correctAnswer;
}

Question.prototype.displayQuestion = function() {
	console.log(this.question);

	for(var i = 0; i < this.answers.length; i++){
		console.log(i + ': ' + this.answers[i]);
	}
}


Question.prototype.checkAnswer = function(answer, callback){
	var sc;
	if(answer == this.correctAnswer){
		console.log('Congrats ! You Won !');
		sc = callback(true);
	} else {
		console.log('Sorry, but it is not the correct answer.');
		sc = callback(false);
	}
	this.displayScore(sc);
}

Question.prototype.displayScore = function(score){
	console.log('You current score is: ' + score);
}

Question.prototype.displayScore = function(score){
	console.log('You current score is: ' + score);
	console.log('--------------------------');
}

function score(){
	var sc = 0;
	return function(correct){
		if(correct){
			sc++
		}
		return sc;
	}
}

var keepScore = score();

var q1, q2, q3;

q1 = new Question('Q1) 5 - 3 ?', [4, 2], 1);

q2 = new Question('Q2) 8 - 2 ?', [4, 2, 6], 2);

q3 = new Question('Q3) 7 + 7 ?', [14, 13], 0);

var questions = [q1, q2, q3];

function nextQuestion(){ 

	var n = Math.floor(Math.random() * questions.length);

	questions[n].displayQuestion();

	var answer = prompt('Insert the answer: ');

	if(answer != 'exit'){

		questions[n].checkAnswer(parseInt(answer), keepScore);
		
		nextQuestion();

		//console.log('You score: ' + keepScore);

	}
}

nextQuestion();
