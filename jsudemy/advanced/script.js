
// Function Constructor

var currentYear = new Date().getFullYear();

const yearOfBirth = 1993;

var lecture1 = false;

if( lecture1 ) {


	var Person = function( name, yearOfBirth, job) {
		this.name = name;
		this.yearOfBirth = yearOfBirth;
		this.job = job;
	};

	// Add methods in the Objects using prototype property

	Person.prototype.calcAge = function() {
		return currentYear - this.yearOfBirth;
	}

	var hugo = new Person( 'Hugo', 2013, 'No info' );
	var carol = new Person( 'Carol', 1993, 'No info' );
	var mark = new Person( 'Mark', 1965, 'Soldier' );

	console.log( hugo.calcAge() );
	console.log( carol.calcAge() );
	console.log( mark.calcAge() );
}


// Object Create 

var lecture2 = false; 

if( lecture2 ) {

	var Person = function( name, yearOfBirth, job){
		this.name = name;
		this.yearOfBirth = yearOfBirth;
		this.job = job;
	};

	Person.prototype.calcAge = function() {
		return currentYear - this.yearOfBirth;
	}

	Person.prototype.age = function(){
		return this.calcAge();
	};

	var PersonProto = {
		calcAge: function() {
			return currentYear - this.yearOfBirth;
		}
	};

	var johan = Object.create( PersonProto);
	johan.name = 'Johan';
	johan.yearOfBirth = 1993;
	johan.johan = 'No info';

	var marie = Object.create( PersonProto, {
		name: {value: 'Marie'},
		yearOfBirth: {value: 1983},
		job: {value: 'Teacher'}
	});

	console.log( johan );
	console.log( marie );

	var hildo = new Person( 'Hildo', 1993, 'Front-end Developer' );


	console.log( hildo.age() );

}


// Primitives vc Objects

var lecture3 = false;

if( lecture3 ){

	// Primitives only copy the data

	var x = 12;
	var y = x; 
	var x = 21;

	console.log( x, y);

	// Objects create a link between them

	var obj1 = {
		var: 12
	}

	var obj2 = obj1;

	obj1.var = 21;

	console.log( obj1, obj2 );

	// Functions
	// In this case, the primitive is not change, but the object has been chenged

	var age = 13; 
	var obj = {
		city: 'São Paulo'
	}

	function change( a, b ) {
		a = 31;
		b.city = 'Berlin';
	}

	change( age, obj );

	console.log( age, obj );

}


// Function into another function 

var lecture4 = false; 

if( lecture4 ){


	// A function is an instance of the Object type;

	// A function behaves like any other object; 

	// We can store functions in a variable; 

	// We can pass a function as an argument to another function; 

	// We can return a function from a function;


	var years = [ 2013, 1993, 1945, 1942, 1939];

	function arrayCalc( arr,  fn ) {
		var arrRef = [];
		for(var i = 0; i < arr.length; i++){	
			arrRef.push(fn(arr[i]));
		}
		return arrRef;
	}

	function calcAge( el ){
		return currentYear - el;
	}

	function isFullAge( el ){
		return el >= 18
	}

	function maxHeartRate( el ){
		if( el >= 18 && el <= 81 ){
			return 206.9 - ( 0.67 * el );
		} else {
			return -1;
		}
	}

	var ages = arrayCalc( years, calcAge );
	var fullAges = arrayCalc( ages, isFullAge );
	var maxHeart = arrayCalc( ages, maxHeartRate);

	console.log( ages, fullAges, maxHeart ); 


}


// Functions returnin functions

var lecture5 = false;

if( lecture5 ){

	function interviewQuestion( job ){
		if( job === 'designer' ){
			return function( name ){
				console.log( name + ', can you explain what UX design is?' );
			}
		} else if ( job === 'teacher' ){
			return function( name ){ 
				console.log( 'What subject you teach, ' + name + ' ?' );
			}
		} else {
			return function( name ) {
				console.log( 'Hello ' + name + '! What do you do?' );
			}
		}
	}

	var teacherQuestion = interviewQuestion( 'teacher' );

	var designerQuestion = interviewQuestion ( 'designer' );

	var genericQuestion = interviewQuestion( 'generic' );

	teacherQuestion( 'Eddy' );

	designerQuestion( 'Fredie' );

	genericQuestion( 'Alice' );


	// you can also call the fuction in this way

	interviewQuestion( 'designer' )( 'Aline' );
	interviewQuestion( 'teacher' )( 'Joana' );
	interviewQuestion( 'generic' )( 'James' );

	// you do not need store the function return in a variable in this way;

}

var lecture6 = false; 

// The IIFE

if( lecture6 ){

	// imagine the situation bellow and think so, 
	// to display the result of the function, we need call the function, 
	// but if you don't need a function ( piece of code what can be reuse ), try use a expression


	// function mode exemple
	function game(){
		var score = Math.random() * 10;
		console.log( score >= 5 );
	}

	game();


	// now, we create a expression, insert the function in parenteses, where can be executed 
	// without a name or a call;
	// in the first paranteses, we insert the expression, and at the second, 
	// we give the parameters 

	(function (goodluck){
		var score = Math.random() * 10;
		console.log( 'This is the a expression: ', score >= 5 - goodluck );
	})(5);

	// we use this to data privacity. 

}


// Closures

var lecture7 = false; 

if(lecture7){

	function retirement(retirementAge){
		return function(yearOfBirth){
			return retirementAge - (currentYear - yearOfBirth);
		}
	}

	console.log(retirement(65)(yearOfBirth), 'until retirement.');

	var retirementFR = retirement(62);
	var retirementDE = retirement(65);
	var retirementHU = retirement(63);
	var retirementRU = retirement(60);

	console.log('If you live in Germany, you have ', retirementDE(yearOfBirth), ' until retirement.');
	console.log('If you live in Fance, you have ', retirementFR(yearOfBirth), ' until retirement.');
	console.log('If you live in Hungary, you have ', retirementHU(yearOfBirth), ' until retirement.');
	console.log('If you live in Russia, you have ', retirementRU(yearOfBirth), ' until retirement.');

	// the magic of the closures is when the execution scope of a function end, you can storege the
	// varibles in an object, to be use after, like above

	console.log('\n-------------------------------\n\n');
	
	// small chalenge 

	function questionInterview(job){
		return function(name){
			if(job === 'front-end'){
				return function(money){
					console.log(name + ' the salary is ' + money);
				}
				//console.log('Nice ' + name + '! Show me some site.');
			} else if(job === 'fullstack'){
				console.log('That is great ' + name + '! Tell me what tecnology you have domain.');
			} else {
				console.log('So ' + name + ', what do you do?');
			}
		}
	}

	var frontend = questionInterview('front-end');
	var fullstack = questionInterview('fullstack');
	var another = questionInterview('another');

	fullstack('Hildo');
	frontend('Hugo')(5000);
	another('Kirki');

	// three functions 
	questionInterview('front-end')('Hugo')(3000);

}


// Bind, call and apply

if(lecture8 = true) {
	
	var hildo = {
		name: 'Hildo',
		age: currentYear - yearOfBirth,
		job: 'Front-end Developer',
		presentation: function(style, momentOfTheDay){
			if(style === 'formal'){
				console.log('Formal presentation.' +
					'\n Moment: ' + momentOfTheDay +
					'\n Name: ' + this.name +
					'\n Age: ' + this.age +
					'\n Job: ' + this.job
					);
			} else if (style === 'informal') {
				console.log(
					'Informal presentation.' +
					'\n Moment: ' + momentOfTheDay +
					'\n Name: ' + this.name +
					'\n Age: ' + this.age +
					'\n Job: ' + this.job
					);
			} else {
				console.log('Nothing mach.');
			}
		}
	};

/*
	var hildo.prototype.presentation = function(style, momentOfTheDay){
		if(style == 'formal'){
			console.log(
				'Formal presentation.' +
				'\n Moment: ' + momentOfTheDay +
				'\n Name: ' + this.name +
				'\n Age: ' + this.age +
				'\n Job: ' + this.job
				);
		} else if (style == 'informal') {
			console.log(
				'Informal presentation.' +
				'\n Moment: ' + momentOfTheDay +
				'\n Name: ' + this.name +
				'\n Age: ' + this.age +
				'\n Job: ' + this.job
				);
		} else {
			console.log('Nothing mach.')
		}
	};

*/
	hildo.presentation('formal', 'night');
	hildo.presentation('informal', 'afternoon');

	var hugo = {
		name: 'Hugo',
		age: 4,
		job: 'Be Cool'
	};

	hildo.presentation.call(hugo, 'informal', 'afternoon');


	// with bind you can store a function to stup by defaut some parameters and insert another when you whant; 
	// bellow we create a function with some arguments setup, only use the moment before;

	var hildoInformal = hildo.presentation.bind(hildo, 'informal');
	hildoInformal('night');

}

