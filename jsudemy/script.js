/*
-------------------------------------
	1. Variables
-------------------------------------
*/

// var name = 'Hildo';

// var lastName = 'Quetz';

// var age = 24;

// var fullAge = true;

// console.log(name+' '+lastName+'. Age: '+age);

/*
-------------------------------------
	2. Variables pt.2
-------------------------------------


var name = 'Hildo';
var age = 24; 
var job = 'Front-end'; 
var isMarried = false;
var hasChildren = true;


var lastName = prompt('Say your last name: '); 

console.log(
	'Name: '+name+' '+lastName+
	'\nAge: '+age+
	'\nJob: '+job+
	'\nisMarried: '+isMarried+
	'\nhasChildren: '+hasChildren
);

alert(
	'Name: '+name+' '+lastName+
	'\nAge: '+age+
	'\nJob: '+job+
	'\nisMarried: '+isMarried+
	'\nhasChildren: '+hasChildren
);

*/

/*
-------------------------------------
	3. Operators 
-------------------------------------
*/


// var currentYear = 2017;

// var birthYear = currentYear - 24;

// console.log(birthYear);

// birthYear = currentYear - 24 * 2;

// console.log(birthYear);

// var result1 = 15;
// var result2 = 15;

// var result2 = result1 = (2 * 3 + 2) - 3; 

// console.log(result2, result1);


/*
-------------------------------------
	4. if and switch 
-------------------------------------
*/

/*
var name = 'Hildo'; 
var age = 24;
var isMarried = false;
var job = prompt('What you do?');


if(isMarried) {
	console.log(name+' is married');
} else {
	console.log(name+' is not married');
}


if (age < 20) {
	console.log(name+' is a teenager');
} else {
	console.log(name+' is a man');
}

switch (job) {
	case 'teacher':
		console.log(name+' is a teacher');
		break;
	case 'front-end':
		console.log(name+' is a front-end');
		break;
	default:
		console.log(name+' not say for us');
		break;
}

*/

/*
-------------------------------------
	Code chalenge
-------------------------------------
*/

/*
// Player 1

var nameP1 = prompt('Name for player 1:');
var heightP1 = prompt('Height for player 1:');
var ageP1 = prompt('Age for player 1:');

// Player 2

var nameP2 = prompt('Name for player 2:');
var heightP2 = prompt('Height for player 2:');
var ageP2 = prompt('Age for player 2:');

// Player 3

var nameP3 = prompt('Name for player 3:');
var heightP3 = prompt('Height for player 3:');
var ageP3 = prompt('Age for player 3:');


// Sum 

var p1 = heightP1 + 5 * ageP1;
var p2 = heightP2 + 5 * ageP2;
var p3 = heightP3 + 5 * ageP3;




// Compare
if(p1 > p2 && p1 > p3 ) {
	alert(nameP1+' win wich '+p1+' points.');
	console.log(nameP1+' win wich '+p1+' points.');
} else if (p2 > p1 && p2 > p3) {
	alert(nameP2+' win wich '+p2+' points.');
	console.log(nameP2+' win wich '+p2+' points.');
} else {
	alert(nameP3+' win wich '+p3+' points.');
	console.log(nameP3+' win wich '+p3+' points.');
}

console.log(
	'The result\n'+
	nameP1+' = '+p1+'\n'+
	nameP2+' = '+p2+'\n'+
	nameP3+' = '+p3
);
*/



function separator( string ) {
	if ( string ) {
		console.log('---------------- ' + string + ' --------------------');
	} else {
		console.log('------------------------------------');
	}
}


/*
-------------------------------------
	5. functions 
-------------------------------------
*/

var lecture5 = false;

if (lecture5) {

	function calcAge (yearOfBirth, currentYear) {
		var age = currentYear - yearOfBirth;
		return age;
	}

	// use return
	console.log(calcAge(1993,2017));

	function person (name, yearOfBirth, currentYear) {
		var age = currentYear - yearOfBirth;
		console.log(name, age);
	}

	// NO use return
	person('Hildo', 1993, 2017);
}

/*
-------------------------------------
	7. arrays 
-------------------------------------
*/

var lecture7 = false; 

if ( lecture7 ) {

	// the array mode 1
	var names = ['HQ', 'CP', 'AC'];

	// the array mode 2
	var years = new Array(1993, 1994, 1962);

	// display data
	console.log( names );
	console.log( years );
	console.log( names[0], years[0] );

	// new attribution
	names[0] = 'Hugo';
	console.log( names[0] );


	// the push() function add a new data in the end of array 
	names.push( 'New name' );
	names.push( 'Another' );

	console.log( names );

	// the unshift() function add new data on begining of the array
	names.unshift( 'The first' );

	console.log( names );

	// the pop() function remove data in the end of the array
	names.pop();

	console.log( names );

	// the indexOf() return the position of the data ojn the array

	console.log( names.indexOf( 'Hugo' ) );

	// the indexOf(), case don's find the data search, return -1 by default

	if( names.indexOf( 'Hugo' ) == -1 ) {
		console.log( 'Don\'t find' );
	} else {
		console.log( 'We find' );
	}

}


/*
-------------------------------------
	7. Objects
-------------------------------------
*/

var lecture8 = false;

if( lecture8 ) {

	// create a object

	var hq = {
		name: 'Hildo',
		lastName: 'Quetz',
		yearOfBirth: 1993,
		job: 'Front-end',
		isMarried: false
	}

	// call a objetct

	console.log( hq.lastName );
	console.log( hq['name'] );

	// attribution

	hq.name = 'João';
	console.log( hq.name, hq.lastName );

	// show all data of object 
	console.log( hq );

	// another form to cratea a object 

	var newObject = new Object();
	newObject.a = 'You select A';
	newObject.b = 'You select B';
	newObject.c = 'You select C';

	console.log( newObject );

}

/*
-------------------------------------
	9. Objects and Methods
-------------------------------------
*/

var lecture9 = false;

if ( lecture9 ) {

	var person = {
		name: 'Hildo',
		lastName: 'Quetz',
		yearOfBirth: 1993,
		job: 'Front-end',
		isMarried: false,
		family: ['Hugo', 'Carol', 'Silvana'],

		// Method of the object, calc age
		age: function() {
			return 2017 - this.yearOfBirth;
		}
	};

	// Call an method into the object witch object parameters mod 1

	console.log(person.age());

	// Call an array into the object
	
	console.log(person.family[0] + ' ' + person.lastName);


	// Call an method into the object witch object parameters mod 2

	var animal = {
		type: 'Cat',
		birth: 2013,
		age: function() {
			return 2017 - this.birth;
		}
	};

	console.log(animal.age());


	// Create a variable into the method (see bellow 'howOld')

	var car = {
		brand: 'Mercedes',
		model: 'C130',
		year: 2003,
		calcOld: function() {
			this.howOld = 2017 - this.year;
		}
	};

	// call the method before use the result
	car.calcOld();
	console.log(car.howOld);

}


/*
-------------------------------------
	10. Loops
-------------------------------------
*/

var lecture10 = false;

if ( lecture10 ) {

	for( var i = 0; i < 10; i++ ) {
		console.log(i);
	}

	separator();

	var carBrandings = ['Mercedes Benz', 'BMW', 'Audi', 'Lamborghini', 'Rolls-Royce'];

	for ( var x = 0; x < carBrandings.length; x++ ) {
		console.log(carBrandings[x]);
	}

	separator('Invert order');

	for ( var x = (carBrandings.length - 1); x > -1; x-- ) {
		console.log(carBrandings[x]);
	}

	separator('Break the Loops');

	for( var y = 1; y <= 5; y++ ){
		console.log(y);
		if ( y == 3 ) {
			break;
		}
	}

	separator('Continue the Loops');

	for( var y = 1; y <= 5; y++ ){
		if ( y == 3 ) {
			continue;
		} else {
			console.log(y);
		}
	}

	var useThis = false;

	if (useThis) {
		var prove = true;
		var num = 0;

		// only if the condition is true
		for(;prove;) {
			console.log( num += 1 );
			if ( num > 10 ) {
				prove = false;
			}
		}
	}
}


/*
-------------------------------------
	2 - Code Challenge
-------------------------------------
*/

var codeChallenge2 = false;

if ( codeChallenge2 ) {

	var years = [1990, 1998, 2013, 2000, 1978, 1980, 2008, 2003];
	
	var testYears = [];

	function check18( year ) {
		
		var check; 

		year = 2017 - year;
		
		if ( year >= 18) {
			check = true;
		} else {
			check = false;
		}

		return check;

	}

	for( var x = 0; x < years.length; x++ ) {

		testYears[x] = check18(years[x]);

		if (testYears[x]) {
			console.log('You born in ' + years[x] + ' and have ' + ( 2017 - years[x] ) + ' years old, so you can pass...');
		} else {
			console.log('Sorry, you born in ' + years[x] + ' and have ' + ( 2017-years[x] ) + ' years old, you can not pass!');
		}
	}

}

/*
-------------------------------------
	JS Languase Basicis, ES5, ES6, ES2015, ES2016 
-------------------------------------
*/

/*

	ES5 is the most accepted version of js for browsers 

*/




var lecture11 = false;

if ( lecture11 ) {

	// How JS Works ? 

	/*

	To work on browser, js follow this steps

	1. Parser
		Check the syntax of code, if it's ok, go to the next level; 

	2. Conversion to machine code
		Now, the engine of JS on your browser gonna convert the js code on a code of can be read by your machine processor

	3. Run the code
		Finaly the code can be executed by your machine;

	*/

}

/*
-------------------------------------
	Execution contexts
-------------------------------------
*/

var lecture12 = false;

if ( lecture12 ) {

	/*
	
	Execution Context
		Is a box where the code execution is isolated ( a function into another function or private method )

	Global Context
		Is the code ( function or variable ) what can be called anywhere

	*/

	function globalExecution() {
		
		console.log( "The global function are called" );
		
		function contextExecution() {
			console.log( "Now, the context function are called, and can not be called by global context." );
			console.log( "If you try call the context function on global, you receive an error." );
		}

		contextExecution();
	}

	globalExecution();

}

/*
-------------------------------------
	Hoisting
-------------------------------------
*/

var lecture13 = false;

if ( lecture13 ) {

	/*
		In this example, we called the function before make it, and work!

		It work for declared functions, only. 
	*/

	calcAge(1993);

	function calcAge( year ) {
		return console.log( 2017 - year );

	}

	var a = "a"; 
	console.log( "global - try call a = " + a );
	// console.log( "global - try call b = " + b ); - this return error
	// console.log( "global - try call c = " + c ); - this return error

	function tryContext() {
		var b = "b";
		console.log( "S1 - try call a = " + a );
		console.log( "S1 - try call b = " + b );
		//console.log( "S1 - try call c = " + c ); - this return error

		function two() {
			var c = "c";
			console.log( "S2 - try call a = " + a );
			console.log( "S2 - try call b = " + b );
			console.log( "S2 - try call c = " + c );
		}
		two();
	}

	tryContext();

}


/*
-------------------------------------
	The 'this' variable
-------------------------------------
*/

// is used in regular function to call a variable of the global execution, useful for variables with the same name;

var lecture14 = true;

if ( lecture14 ) {
	
	var sameName = "I do not see.";

	function context() {

		var sameName = "I see you.";

		console.log( sameName );
		console.log( this.sameName );

	}

	context();

	// in the method call, this variable points to the object that is calling the method;

	var newObject = {
		name: "Hugo Quetz",
		newName: function ( string ) {
			this.name = string;
		}
	};

	console.log( newObject.name );

	newObject.newName( "Hildo Quetz" );

	console.log( newObject.name );

	// you can also import a method of another object

	var anotherObject = {
		name: "Clemente",
	}

	anotherObject.newName = newObject.newName;

	anotherObject.newName( "Custódio" );

	console.log( anotherObject.name );

}

