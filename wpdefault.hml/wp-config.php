<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wpdef_hml');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '130293');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(_7yu-$&S;(NZt$%3^]!l0|AB-x+2;YN(k%.HYp/K2qRIX{59fcI9fl,+:+=n-3,');
define('SECURE_AUTH_KEY',  '_r[fgwRunM}|j4;-OguKg2k];}G~S}n&~N>%PE,ySd})$(yE?gV4d-I9{=is!Ca)');
define('LOGGED_IN_KEY',    'KY)pRL7FU*$JB q*_4f}aTA] #d,%er~;jM9si6m+|U@#vN`IDL+)JczY%U0mb7,');
define('NONCE_KEY',        '(9jvo`?%~dAtq_Mgoyy-UTms;Zqn2&oOmc= ,OS02d$q~&Z`7+$iTf_x);+%v(lo');
define('AUTH_SALT',        'OCuKH8aP#29{47t{1h{rZ:xS+7E8@*|W&iOi#FQu)rp)]U28Eb%5G- 8SWf3q7l]');
define('SECURE_AUTH_SALT', '6lM#N3uR#8C,kM&$4n`V4 5^e=F`Aw4!G0M 6_`Sq+^,gJdyHjbETaTx*+W_nQ!t');
define('LOGGED_IN_SALT',   'Vm&mD?glX?jW:6Q;|jQ)c&d3l#H6j]fX9/o3iv(=g4%`YKg)rrq58dTh#~C*um9n');
define('NONCE_SALT',       ' x-dM9(jt5!MDVnVrDZuJ#wW-?B:!?l%B!0x$HgNVC^vkVLEpET62r8R^7e851ND');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
