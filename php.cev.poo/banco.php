<?php 
/*
	Exercício de Fixação 

	OBJETIVO: Implementar um objeto conta. 

	x- Testar com duas pessoas ( Hugo ( cc 300 ) e Carol ( cp 400 ) )
	X- Dois tipos de conta: corrente e poupança
	x- métodos get e set para todos os atributos
	x- costrutor p/ nova conta indica saldo zero e status inativo

	Atributos
		+numConta
		gs - #tipo
		-detentor
		gs -saldo
		-status( ativo, inativo )

	Métodos
		x abrirConta() - if cp +50 else +150
		x fecharConta()
		x - depositar() // conta tem q estar ativa 
		x - sacar() // conta tem q estar ativa e ter saldo suficiente
		x - mensalidade() // chamou, cobrou. cc 12, cp 20

*/

class Conta {
	public $numConta;
	protected $tipo;
	private $detentor;
	private $saldo;
	private $ativo;

	function Conta() {
		$this->ativo = false;
		$this->saldo = 0.00;
	}


	// Ativo
	
	function getAtivo() {
		echo ($this->ativo == true ? "\nATIVA\n" : "\nINATIVA\n" );
	}

	function setAtivo( $ativo ) {
		switch( $ativo ) {
			case true:
				$this->ativo = true;
				return $this->getAtivo();
				break;

			case false:
				$this->ativo = false;
				return $this->getAtivo();
				break;

			default:
				echo "\nTipo desconhecido.\n";
				break;
		}
	}


	// Saldo

	function getSaldo() {
		echo "\nSaldo da conta R$ " . $this->saldo . "\n";
	}

	function setSaldo( $valor, $op ) {
		$saldo = $this->saldo;

		if( $op == "CREDITO" ) {
			$this->saldo += $valor;
			return "\nOperação realizada, novo saldo: " . $this->getSaldo() . "\n";
			
		} else {
			if( $saldo < $valor ) {
				return "\nSaldo insuficiente! Operação não realizada.\n";
			} else {
				$this->saldo -= $valor;
				return "\nOperação realizada, novo saldo: " . $this->getSaldo() . "\n";
			}
		}
		
	}


	// Tipo

	function getTipo() {
		return $this->tipo;
	}

	function setTipo( $tipo ) {
		switch ( $tipo ) {
			case 'cp':
				$this->tipo = $tipo;
				break;

			case 'cc':
				$this->tipo = $tipo;
				break;
			
			default:
				return "\nTipo não compreendido.\n";
				break;
		}
	}


	// Número da Conta

	function getNumConta() {
		if( isset( $this->numConta ) ) {
			echo "\nNúmero da conta: $this->numConta\n";
		} else {
			echo "\nNúmero da conta ainda não definido.\n";
		}
	}

	function setNumConta() {
		if ( isset( $this->numConta ) ) {
			echo "\nO número da conta já esta definido.\n";
			$this->getNumConta();
		} else {
			$this->numConta = rand( 1000, 9999 );
			$contas[] = $this->numConta;
			echo "\nNovo número da conta definido.\n";
			$this->getNumConta();
		}
	}


	// Detentor

	function getDetentor() {
		echo ( $this->detentor != false ? "\n$this->detentor\n" : "\nDetentor da conta não definido.\n" );
	}

	function setDetentor( $nome ) {
		$this->detentor = $nome;
		echo "\nNovo detentor da conta: $this->detentor\n";
	}

	function depositar( $valor ) {
		$op = ( $valor < 1 ? false : true );

		if( $this->ativo ) {
			if( $op ) {
				$this->setSaldo( $valor, "CREDITO" );
				return "\nDepósito realizado com sucesso, novo saldo: " . $this->getSaldo() . "\n";
			} else {
				return "\nDepósito não realizado, valor menor que 1.\n";
			}
		} else {
			echo "\nNão é possível realizar depósito em uma conta inativa.\n";
		}

	}

	function abrirConta( $detentor, $tipoConta ) {

		$this->setTipo( $tipoConta );
		$this->setAtivo( true );
		$this->setDetentor( $detentor );
		$this->setNumConta();

		switch ( $tipoConta ) {
			case 'cp':
				$this->depositar( 50 );
				break;

			case 'cc':
				$this->depositar( 150 );
				break;

			default:
				echo "\nTipo de conta não definido.\n";
				break;
		}


		echo "\nNova conta criada com sucesso! \n";

	}

	function fecharConta( $numConta ) {

		if ( $numConta == $this->numConta ) {
			if ( $this->saldo > 0 ) {
				echo "\nNão é possível encerrar uma conta com saldo. Favor proceder com o saque.\n";
				$this->getSaldo();
			} else {
				$this->setAtivo( false );
				echo "\nConta encerrada com sucesso!\n";
			}
		}

	}

	function sacar( $valor ) {
		$op = ( $valor > $this->saldo ? false : true );

		if( $op ) {
			$this->setSaldo( $valor, "DEBITO" );
			return "\nSaque realizado! Novo saldo: " . $this->getSaldo() . "\n";
		} else {
			return "\nSaque não permitido, valor superior ao saldo ( " . $this->getSaldo() . " )\n";
		}
	}

	function mensalidade() {
		if( $this->tipo == "cp" ) {
			$valorCobranca = 20;
			$this->setSaldo( $valorCobranca, "DEBITO" );
			return "\nDescontado R$ $valorCobranca de mensalidade.\n";
		} else {
			$valorCobranca = 12;
			$this->setSaldo( $valorCobranca, "DEBITO" );
			return "\nDescontado R$ $valorCobranca de mensalidade.\n";
		}
	}

}

$x = new Conta; 

$y = new Conta;

$x->abrirConta( "Hugo Quetz", "cc" );

$y->abrirConta( "Ingrid Caroline", "cp" );

$x->depositar( 500 );

$x->depositar( 300 );

$x->sacar( 100 );

$x->mensalidade();

$y->mensalidade();

print_r( $x );

print_r( $y );