<?php

class Calc {

	// INPUT DO USUÁRIO

	var $cmm; // Consumo Médio Mensal
	var $uf;
	var $cidade; 
	var $tipoImovel; // Comercial ou Residencial
	var $concessionaria;


	// CMM

	function setCMM($value) {
		$this->cmm = $value;
	}

	function getCMM() {
		return $this->cmm;
	}

	// UF

	function setUF($value) {
		$this->uf = $value;
	}

	function getUF() {
		return $this->uf;
	}

	// CIDADE

	function setCIDADE($value) {
		$this->cidade = $value;
	}

	function getCIDADE() {
		return $this->cidade;
	}

	// TIPO IMOVEL

	function setTIPOIMOVEL($value) {
		$this->tipoImovel = $value;
	}

	function getTIPOIMOVEL() {
		return $this->tipoImovel;
	}

	// CONCESSIONARIA

	function setCONCESSIONARIA($value) {
		$this->concessionaria = $value;
	}

	function getCONCESSIONARIA() {
		return $this->concessionaria;
	}

}

$x = new Calc;

$x->setCMM(1000);
$x->setCIDADE('São Paulo');
echo $x->getCMM() . ' ' . $x->getCIDADE();



	