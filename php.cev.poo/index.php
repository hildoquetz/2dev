<?php 
	
/* 
	Programação Orientada a Objeto - Curso em Vídeo
	url: https://www.youtube.com/watch?v=aR7CKNFECx0&index=3&list=PLHz_AreHm4dmGuLII3tsvryMMD7VgcT7x
*/

function cev(){
	echo "https://www.youtube.com/watch?v=aR7CKNFECx0&index=3&list=PLHz_AreHm4dmGuLII3tsvryMMD7VgcT7x";
}

/*
	--------------------------------------
		Aula 1 - O que é objeto? 
	--------------------------------------
*/

/*
	Objeto: Coisa material ou abstrata que pode ser percebida pelos sentidos e descritas por meio das suas características, comportamentos e estado atual. 

	Todo Objeto é composto por 3 pilares

	Atributos

	Métodos

	Estado
	
	Classe: define os atributos e metodos comuns que serão compartilhados por um objeto. 

	Todo objeto pertence/deriva a uma classe. 

	Podemos entender a classe como sendo o objeto modelo, e os objetos como devidações isoladas da classe. Ex. de novo objeto: 

	c1 = new Caneta; 

	Agora 'c1' é um objeto, que deriva da classe caneta. Quando criamos um novo objeto, dizemos que estamos 'instanciando' este objeto.  


	Exercício de Objeto

	Objeto Mouse
		Atributos
			Cor 	= preto
			botões	= 5
			conexao	= usb
			led 	= true
		Métodos
			Clique Direito 
			Clique Esquerdo
			Scroll
		Estado 
			Cor 	= preto
			botões	= 2
			conexao	= bluethooth
			led 	= false	


	--------------------------------------
		Aula 2 - Visibilidade do Objeto 
	--------------------------------------
	

	+ público
	- privado
	# protegido

	+ público
		Todas as classes podem utilizar

	- privado
		Somente a classe pode utilizar

	# protegido
		Somente a classe mãe e seus filhos pode utilizar

	Ao tentar acessar externamente um atributo privado ocorre um erro fatal, pois somente a classe pode alterá-lo. Desta forma é necessário utilizar métodos acessores (get, set, construct).


	--------------------------------------
		Aula 3 - Metodos especiais
	--------------------------------------
	
	Métodos Acessotes (Getter Methods)
		Forma de protecer a leitura de atributos protegidos de uma classe

	Métodos Modificadores (Setters Methods)
		Forma protegida de alterar os atributos protegidos de uma classe
	
	Método Construtor (Construct Method)
		Executa uma cadeia de métodos sempre que um objeto é instanciado

	Para criar um constror no php, use:

	cass minhaClasse {
		var $atr1;
		var $atr2;
		var $atr3;

		public functrion minhaClasse() {
			$this->atr1 = "string";
			$this->atr2 = true;
			$this->atr9 = 9;
		}
	}

	Foi utilizado o próprio nome da classe para criar o construtor, o método antigo era utilizar a função global '__construct', confira: 

	cass minhaClasse {
		var $atr1;
		var $atr2;
		var $atr3;

		public functrion __construct() {
			$this->atr1 = "string";
			$this->atr2 = true;
			$this->atr9 = 9;
		}
	}


	--------------------------------------
		Aula 4 - Exercício Banco
	--------------------------------------
	

	Executado com sucesso ! 


	--------------------------------------
		Aula 5 - Pilares do POO
	--------------------------------------
	
	AEHP
	
	
	Encapsulamento
	Herança
	Polimorfismo




*/

echo "\n https://www.youtube.com/watch?v=0G566D5qGH8&index=8&list=PLHz_AreHm4dmGuLII3tsvryMMD7VgcT7x \n";

class Mouse {
	var $cor;
	var $botoes;
	var $conexao;
	var $led;

	function clickDir() {
		echo "\nVocê clicou com o direito...\n";
	}

	function clickEsq() {
		echo "\nVocê clicou com o esquerdo...\n";
	}

	function scroll() {
		echo "\nVocê está utilizando o scroll...\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.";
	}

}


class Caneta {
	public $modelo;
	public $cor;
	private $ponta;
	private $carga;
	protected $tampada;


	public function Caneta(){
		$this->tampar();
		$this->cor = "Azul";
		$this->modelo = "Bic Prateada";
		$this->ponta = 1.0;
		$this->carga = "100%";
	}

	public function rabiscar() {
		if( !$this->tampada ) {
			echo "\nEstou rabiscando...\n";
		} else {
			echo "\nA caneta está tampada, é preciso destampar...";
			$this->destampar();
			$this->rabiscar();
		}
	}

	public function tampar() {
		$this->tampada = true;
		echo "Tampando a caneta ...\nCaneta tampada!\n";
	}

	public function destampar() {
		$this->tampada = false;
		echo "Destampando a caneta ...\nCaneta destampada!\n";	
	}

	public function getModelo() {
		return $this->modelo;
	}

	public function setModelo($string) {
		$this->modelo = $string;
		return "\nSet " . $this->modelo . "\n";
	}
}

	$gamer = new Mouse;
	$gamer->cor = "azul";
	$gamer->botoes = 5;
	$gamer->led = true;

	$gamer->clickDir();
	$gamer->scroll();

	echo "\n----------------------\n";
 
	$c1 = new Caneta;
	
	// ao tentar acessar o atributo $tampada, ocorre erro fatal por ser um atributo protegido
	
	//$c1->tampada = true;

	// para acessar um atributo privado, é preciso utilizar metodos de acesso, como a função destampar() que é pública

	$c1->tampar();

	print_r( $c1 );

	$c1->destampar();

	print_r( $c1 );

	$c1->rabiscar();

	$c1->modelo = "Bic Cristal";

	echo $c1->getModelo();

	echo $c1->setModelo("Bic Premium");

	$c2 = new Caneta;

	print_r( $c2 );

	// print_r( $c1 );
	// print_r( $gamer );
	// $gamer->clickDir();
	// $gamer->clickEsq();
	// $gamer->scroll();




?>
