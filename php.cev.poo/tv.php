<?php

class Televisao {
	public $ligada;
	private $volume;
	private $canal;

	public function Televisao() {
		$this->ligada = false;
		$this->volume = 20;
		$this->canal = 1;
		echo "\nNova classe " . __CLASS__ . " criada.";
	}

	public function __destruct() {
		echo "\nClasse destruida.\n";
	}

	public function getLigada(){
		return $this->ligada;
	}

	public function setLigada( $ligada ) {
		
		if ( $this->ligada == $ligada ) {
			echo ( $ligada ? "\nA TV ja está ligada.\n" : "\nA TV já esta desligada \n");
		} elseif ( $ligada ) {
			$this->ligada = true;
		} else {
			$this->ligada = false;
		}

	}

	public function getVolume() {
		return $this->volume;
	}

	public function getCanal() {
		return $this->canal;
	}

	private function mudarCanal( $valor ) {
		$min = 1; 
		$max = 700; 

		if ( $valor <= $max && $valor >= $min ) {
			$this->canal = $valor;
		} else {
			echo "\nCanal não reconhecido!\n";
		}

	}

	private function aumentarVolume( $valor ) {
		$max = 100;

		if ( ( $valor + $this->volume ) <= 100 ) {
			$this->volume += $valor;
		} else {
			$this->volume = 100;
			echo "\nVolume máximo alcançado\n";
		}

		return $this->volume;

	}

	private function baixarVolume( $valor ) {
		$min = 0;
		
		if ( ( $this->volume - $valor ) >= 0 ) {
			$this->volume -= $valor;
		} else {
			$this->volume = 0;
			echo "\nVolume mínimo alcançado\n";
		}

		return $this->volume;

	}

	public function controle( $opcao, $valor ) {
		switch ( $opcao ) {
			case 'aumentarVolume':
				$this->aumentarVolume( $valor );
				break;
			
			case 'baixarVolume':
				$this->baixarVolume( $valor );
				break;

			case 'mudarCanal': 
				$this->mudarCanal( $valor );
				break;

			default:
				echo "\nOpção não reconhecida, tente novamente.\n";
				break;
		}
	} 

}

$lg = new Televisao;

$lg->setLigada( true );

print_r( $lg );

$lg->controle( 'aumentarVolume', 20 );

print_r( $lg );

$lg->controle( 'aumentarVolume', 100 );

$lg->controle( 'baixarVolume', 1000 );

$lg->controle( 'mudarCanal', 333 );

print_r( $lg );


/*

__CLASS__ = Retorna o nome da classe;
__destruct() = Destroi a classe automaticamente ao término do arquivo;
unset( $the_object ) = destroi o objeto; 
__toString() = converte o resultado para string 
*/
