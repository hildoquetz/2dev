#!/bin/bash

	# <command> <arg1> <arg2> ...
	# são aceitos até 9 parametos inicialmente

	echo "Parametro 1: $1";
	echo "Parametro 2: $2";

	# para usar mais de 9 parametros, usamos o 'shift';

	echo "Parametro 3: $3";
	echo "Parametro 4: $4";
	echo "Parametro 5: $5";
	echo "Parametro 6: $6";
	echo "Parametro 7: $7";
	echo "Parametro 8: $8";
	echo "Parametro 9: $9";

	shift 9;

	echo "Parametro 10: $1";
	echo "Parametro 11: $2";