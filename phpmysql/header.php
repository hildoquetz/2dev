<!DOCTYPE html>
<html>
<head>
	<?php include('includes/functions.php'); ?>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>PHP e MySQL Alura</title>
</head>
<body>
	<dir class="container">
		<dir class="primary-container">
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="container">
					<div class="nav-header">
						<a class="navbar-brand" href="index.php">My.Home</a>
					</div>
					<div>
						<ul class="nav navbar-nav">
							<li><a href="form.php">Add Item</a></li>
							<li><a href="list-item.php">Itens List</a></li>
						</ul>
					</div>
				</div>
			</div>