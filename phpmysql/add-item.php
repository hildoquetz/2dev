<?php include("header.php"); ?>
<?php include("includes/db-connect.php"); 
		

			$product = isset($_GET["prod"])?$_GET["prod"]:"";
			$price = isset($_GET["prec"])?$_GET["prec"]:"";
		
			if(insertProduct($product, $price, $connect)) { ?>
				<p class="alert-success">O produto <?= $product ?> no valor de R$ <?= $price ?> foi adicionado com sucesso!</p>
			<?php } else { ?>
				<?php $msgError = mysqli_error($connect); ?>
				<p class="alert-danger">Produto não foi adicionado!<br /> Erro: <?= $msgError ?></p>
			<?php } 
		?>

<?php include("footer.php") ?>