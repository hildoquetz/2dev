<?php  include("header.php"); ?>
            <div class="page-header">
                <h1 class="lobster text-primary">Bootstrap Playground</h1>
            </div>
            <p class="lead">O parque de diversões para quem quer aprender Bootstrap!</p>
            &nbsp;
            &nbsp;
            <h3>Guia de referências</h3>

            <table class="table table-striped">
            	<thead>
            		<tr>
            			<th>Recurso</th>
            			<th>Link</th>
            		</tr>
            	</thead>
            	<tbody>
            		<tr>
            			<td>Bootstrap Official Documentation</td>
            			<td><a target="_blank" href="https://getbootstrap.com/docs/4.0/getting-started/introduction/">https://getbootstrap.com/docs/4.0/getting-started/introduction/</a></td>
            		</tr>
            		<tr>
            			<td>W3Schools Bootstrap Gide</td>
            			<td><a target="_blank" href="https://www.w3schools.com/bootstrap">https://www.w3schools.com/bootstrap</a></td>
            		</tr>
            	</tbody>
            </table>
            &nbsp;
            &nbsp;
            <h3>Outras referências</h3>
            <table class="table table-striped">
            	<thead>
            		<tr>
            			<th>Referência</th>
            			<th>Link</th>
            		</tr>
            	</thead>
            	<tbody>
            		<tr>
            			<td>Boostrap Sidebar Menu</td>
            			<td><a href="https://www.codeply.com/go/GQ1Mz8RqZB" target="_blank">https://www.codeply.com/go/GQ1Mz8RqZB</a></td>
            		</tr>
            	</tbody>
            	
            </table>

<?php  include("footer.php"); ?>