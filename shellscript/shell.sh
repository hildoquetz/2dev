
lession1=false;

if [ $lession1 = true ]; then
	PING=$( ping -w 3 quetz.com.br );
	echo $PING date >> /home/quetz/log.txt;
	ls -l;
fi


# ------------------------------------
#	OPERATORS
# ------------------------------------

# The null operators '-n' and '-z' say when the the variable has value or not;

# String Operators 

#	-n 		string NOT null
#	-z 		string ARE null
# 	= 		string ARE equal
# 	!= 		string NOT equal

# Numbers Operators

# 	-lt		number LESS THEN ...
# 	-gt 	number GREAT THEN ...
#	-le 	number LESS or EQUAL ...
# 	-ge 	number GREAT or EQUAL ...
# 	-eq 	number EQUAL ...
# 	-ne 	number NOT EQUAL ...


lession2=false;

if [ $lession2 = true ]; then	

	echo "The string operators: ";

	num1="";
	num2=01;

	if [ -z $num1 ]; then
		echo "Not value on variable \$num1";
	fi

	if [ -n $num2 ]; then
		echo "The variable \$num2 have value";
	fi

	# Now, with the esle startment
	
	if [ -z $num1 ]; then
		echo "Not value on variable \$num1";
	else 
		echo "else: have value."
	fi

	if [ -n $num2 ]; then
		echo "The variable \$num2 have value";
	fi

	echo "The numbers operators";

	n1=10;
	n2=5;

	echo "Use \$n1=$n1 and \$n2=$n2";

	if [ $n1 -lt $n2 ]; then 
		echo "n1 less then n2";
	else 
		echo "n2 less then n1";
	fi

	if [ $n1 -gt $n2 ]; then 
		echo "n1 great then n2";
	else
		echo "n2 great then n1";
	fi

	if [ $n1 -le $n2 ]; then
		echo "n1 less or equal then n2";
	else
		echo "n2 less or equal then n1";
	fi

	if [ $n1 -ge $n2 ]; then
		echo "n1 great or equal then n2";
	else
		echo "n2 great or equal then n1";
	fi

	if [ $n1 -eq $n2 ]; then
		echo "n1 is equal n2";
	else
		echo "n1 is not equal n2";
	fi

	if [ $n1 -ne $n2 ]; then
		echo "n1 is not equal n2";
	else
		echo "n1 is equal n2";
	fi


fi

lession3=false;

if [ $lession3 = true ]; then
	# operator and = -a
	# operator or = -o
	
	num1=1;
	num2=1;
	num3=2;
	num4=3;

	if [ $num1 -eq $num2 -a $num4 -gt $num3 ]; then
		echo "all true";
	else 
		echo "something false";
	fi

	if [ $num1 -ne $num2 -o $num4 -lt $num3 ]; then
		echo "something true";
	else 
		echo "all false";
	fi

fi


exercice2=false;

if [ $exercice2 = true ]; then

	# a(Ferrari, Lamborghini, Mercedes) 
	# b(Land Hover, Maserati)
	# c(Volks Wagen, Fiat, Ford)

	read -p "Entry the car brand: " entry;

	# Class A
	if [ "$entry" = "Ferrari" -o "$entry" = "Lamborghini" -o "$entry" = "Mercedes" ]; then
		echo "This is an A Class";
	else

		# Class B
		if [ "$entry" = "Land Hover" -o "$entry" = "Maserati" ]; then
			echo "This is an B Class";
		else

			# Class C
			if [ "$entry" = "Volks Wagen" -o "$entry" = "Fiat" -o "$entry" = "Ford" ]; then
				echo "This is an C Class";
			else
				echo "No mach for your search, please try again";
			fi

		fi

	fi

	sleep 2;

	echo "Good Bye ...";

	sleep 1;

	exit;

fi

# ------------------------------------
#	LOOPS
# ------------------------------------

lession4=true;

if [ $lession4 = true ]; then

	# The for loop 

	# execute commands while the condition is true

	exec1=true;

	if [ $exec1 = true ]; then
		for (( i=0; i<=3; i++ )); do
			echo "$i";
		done
	fi

	# The while loop

	# execute commands while the condition is true

	loop=0;

	while [ $loop -le 5 ]; do
		echo $loop;
		loop=$[ $loop + 1 ];
	done

	param1=false;
	param2=true;

	while [ $param2 = true -a $param1 = false ]; do
		log=$[ $log + 1];
		echo "Log " $log;
		if [ $log -gt 10 ]; then
			param1=true;
		fi
	done

	log=0;

	# The until loop

	# execute commands until the condition is true;

	until [ $param1 = false ]; do
		log=$[ $log + 1 ];
		echo "Until log = $log | param status = $param1";
		if [ $log -gt 10 ]; then
			param1=false;
		fi
	done


fi