<?php

function verificar(&$item) {
	while ($item < 1 || $item > 5) {
		echo "\n Opção incorreta, digite novamente: ";
		$item = stream_get_line(STDIN, 1024, PHP_EOL);
	}
}

function item($item, $menu) {
	verificar($item);

	switch ($item) {
		case 1:
			echo "\n\n Você chamou o $menu[1]";
			voltar($menu);
			break;
		case 2:
			echo "\n\n Você chamou o $menu[2]";
			voltar($menu);
			break;
		case 3:
			echo "\n\n Você chamou o $menu[3]";
			voltar($menu);
			break;
		case 4:
			echo "\n\n Você chamou o $menu[4]";
			voltar($menu);
			break;
		case 5:
			echo "\n\n Você optou por $menu[5]\n\n Obrigado!\n";										
	}
}

function opcao($menu) {
	echo "\n\n -------------------------\n\n Digite a opção desejada: ";
	$item = stream_get_line(STDIN, 1024, PHP_EOL);
	item($item, $menu);
}

function menu($menu) {
	$menu = array(
		'1' => "Programa A", 
		'2' => "Programa B",
		'3' => "Programa C",
		'4' => "Programa D",   
		'5' => "Sair", 
		);
	echo "\n -------------------------\n";
	foreach ($menu as $key => $value) {
		echo "\n --> Digite $key para $value.";
	}
	opcao($menu);
}

function voltar($menu) {
	echo "\n\n Digite 9 para voltar: ";
	$op = stream_get_line(STDIN, 1024, PHP_EOL);
	while ($op != 9) {
		echo "\n Opção incorreta, digite novamente: ";
		$op = stream_get_line(STDIN, 1024, PHP_EOL);
	}
	menu($menu);
}

menu($menu=array()); 