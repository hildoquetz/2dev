<?php
	function check (&$a) {
		while ($a < 1 || $a > 10) {
			echo "\n Por favor, informe um número de 1 até 10.\n Tente novamente: ";
			$a=stream_get_line(STDIN, 1024, PHP_EOL);
		}
	}

	function input (&$a,&$b,&$c,&$d) {

		echo "\n Informe N1: ";
		$a = stream_get_line(STDIN, 1024, PHP_EOL);
		check($a);

		echo "\n Informe N2: ";
		$b = stream_get_line(STDIN, 1024, PHP_EOL);
		check($b);

		echo "\n Informe N3: ";
		$c = stream_get_line(STDIN, 1024, PHP_EOL);
		check($c);

		echo "\n Informe N4: ";
		$d = stream_get_line(STDIN, 1024, PHP_EOL);
		check($d);
	}

	/**
	 * Change the values of var
	 * @param unknown $a first number
	 * @param unknown $b second number
	 */
	function change (&$a, &$b) {
		$c = $a;
		$a = $b;
		$b = $c;
	}

	function result ($a, $b, $c, $d) {
		echo "\n A nova ordem é $a, $b, $c e $d! \n\n Obrigado!\n\n";
	}
?>
