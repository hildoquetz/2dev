<?php

	function hq_exitProg (&$exit) {
		echo "\n ---> Você optou por sair! \n";
		$exit = TRUE;
	}

	function hq_check (&$arrayItem, &$exit) {
		if ($arrayItem == 0 ) {
			hq_exitProg($exit);
		} else {
			while ($arrayItem < 1 || $arrayItem > 10) {
				echo "\n Por favor, informe um número de 1 até 10 para reordenar, ou 0 para sair.\n Tente novamente: ";
				$arrayItem = stream_get_line(STDIN, 1024, PHP_EOL);
				if ($arrayItem == 0) {
					hq_exitProg($exit);
				} 
			}
		}
	}

	function hq_input (&$arrayNumbers, $exit) {
		$i = 0;
		while($exit == FALSE) {
			$i += 1;
			echo "\n Informe N$i: ";
			$arrayNumbers[$i] = stream_get_line(STDIN, 1024, PHP_EOL);
			hq_check($arrayNumbers[$i], $exit);
		}
	}

	function hq_result ($arrayNumbers) {
		echo "\n A nova ordem é: \n";
		foreach ($arrayNumbers as $item) {
			if ($item == 0) {
				unset($item);
			} else {
				echo "\n $item";
			}
		}
		echo "\n\n Obrigado! \n\n";
	}

?>
