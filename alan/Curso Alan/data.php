<!DOCTYPE html>
<html>
	<head>
		<?php require('functions.php'); ?>
		<link rel="stylesheet" type="text/css" href="../phpdev/css/bootstrap.css"/>
		<meta charset="utf-8">
	</head>
	<body>

		<?php 

			$n1=isset($_GET["n1"])?$_GET["n1"]:"";
			$n2=isset($_GET["n2"])?$_GET["n2"]:"";
			$n3=isset($_GET["n3"])?$_GET["n3"]:"";
			$n4=isset($_GET["n4"])?$_GET["n4"]:"";
				
			echo "<button class='btn' onclick='history.go(-1);'>Voltar</button>";

			echo "<br/><br/><b>Ordem digitada:</b><br/>  $n1, $n2, $n3, $n4";

			# Process N1

			if ($n1 > $n2) change($n1,$n2);
			
			if ($n1 > $n3) change($n1,$n3);

			if ($n1 > $n4) change($n1,$n4);

			# Process N2

			if ($n2 > $n3) change($n2,$n3);

			if ($n2 > $n4) change($n2,$n4);

			# Process N3

			if ($n3 > $n4) change($n3,$n4);
			
			echo "<br/><br/><b>Reordenado:</b><br/>  $n1, $n2, $n3, $n4";

		?>
	</body>
</html>
