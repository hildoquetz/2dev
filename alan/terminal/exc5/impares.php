<?php 

function entradaDados() {
	$validacao=false;
	while ($validacao==false) {
		
		echo "\n Por favor, informe o primeiro número: ";
		$numIni = stream_get_line(STDIN, 1024, PHP_EOL);
		
		echo "\n Por favor, informe o segundo número: ";
		$numFim = stream_get_line(STDIN, 1024, PHP_EOL);
		
		if($numIni >= $numFim) {
			echo "\n Número inicial deve ser maior que o final.\n";
			$validacao = false;
		} else {
			$validacao = true;
		}
	}
	return $range = array($numIni, $numFim); 
}

function processamento($array) {
	$impar = 0;
	while ($array[0] <= $array[1]) {
		if (($array[0] % 2) != 0) {
			$impar++;
		}
		$array[0]++;
	}
	return $impar;
}

function resultado($result){
	echo "\n\n Há um total de $result números impares";
}

