<?php
	
	require_once('functions.php');

	echo "\n Bem-vindo! \n\n Insira 4 números de 1 a 10.\n\n";

	input($n1,$n2,$n3,$n4);

	// Verify N1
	if ($n1 > $n2) change($n1,$n2);
	if ($n1 > $n3) change($n1,$n3);
	if ($n1 > $n4) change($n1,$n4);
	// Verify N2
	if ($n2 > $n3) change($n2,$n3);
	if ($n2 > $n4) change($n2,$n4);
	// Verify N3
	if ($n3 > $n4) change($n3,$n4);

	result($n1,$n2,$n3,$n4);

?>
