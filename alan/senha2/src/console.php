<?php
`chcp 1252`; //padr�o para caracteres latinos
echo "teste de console: acentua��o\n";
echo "--------------------\n";

`chcp 850`; //o console do DOS usa este cp para a saida de seus comandos
echo "passando parametros\n";
$host = "www.google.com";
echo `ping -n 1 {$host}`;
echo "--------------------\n";

echo "processo como se fosse um arquivo\n";
$handle = popen("dir", "r");
while (($data = fgets($handle)) != null) {
	echo ". $data"; // \n n�o � cess�rios, pois $data j� vem com \n
}
$handle = pclose($handle);
echo "--------------------\n";

echo "ordenamento externo de arquivos\n";
$input = "contatos.txt";
$output = "contatos_ordenado.txt";
`sort {$input} /o {$output}`;
echo `type {$output}`; //imprime o conteudo 
