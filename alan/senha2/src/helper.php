<?php
// constantes para o caracter de fundo
define ( "BG100", "\xDB" );
define ( "BG75", "\xB2" );
define ( "BG50", "\xB1" );
define ( "BG25", "\xB0" );

define ( "SCREEN_X", 0 );
define ( "SCREEN_Y", 0 );
define ( "SCREEN_W", 80 );
define ( "SCREEN_H", 25 );
function color($name) {
	$COLOR_CODES = array (
			"off" => 0,
			"bold" => 1,
			"italic" => 3,
			"underline" => 4,
			"blink" => 5,
			"inverse" => 7,
			"hidden" => 8,
			"black" => 30,
			"red" => 31,
			"green" => 32,
			"yellow" => 33,
			"blue" => 34,
			"magenta" => 35,
			"cyan" => 36,
			"white" => 37,
			"black_bg" => 40,
			"red_bg" => 41,
			"green_bg" => 42,
			"yellow_bg" => 43,
			"blue_bg" => 44,
			"magenta_bg" => 45,
			"cyan_bg" => 46,
			"white_bg" => 47 
	);
	
	return "\033[{$COLOR_CODES[$name]}m";
}
function cls() {
	echo "\033[2J";
}
function at($x, $y, $text = "", $color = "") {
	if (PHP_OS == "WINNT") {
		$x ++; // no PowerShell, a linha 0 fica sob a barra de titulo, por isso a corre��o.
	}
	
	echo "\033[$x;{$y}H"; // isola a variavel. Se colocar $yH, procura pela variavel yH e n�o y
	echo $color . $text . color ( "off" );
}
function border($x, $y, $w, $h, $background = " ", $simple = false) {
	if ($simple) {
		$border = array (
				"lt" => "\xDA",
				"t" => str_repeat ( "\xC4", $w - 2 ),
				"rt" => "\xBF",
				"l" => "\xB3",
				"bg" => str_repeat ( $background, $w - 2 ),
				"r" => "\xB3",
				"lb" => "\xC0",
				"b" => str_repeat ( "\xC4", $w - 2 ),
				"rb" => "\xD9" 
		);
	} else {
		$border = array (
				"lt" => "\xC9",
				"t" => str_repeat ( "\xCD", $w - 2 ),
				"rt" => "\xBB",
				"l" => "\xBA",
				"bg" => str_repeat ( $background, $w - 2 ),
				"r" => "\xBA",
				"lb" => "\xC8",
				"b" => str_repeat ( "\xCD", $w - 2 ),
				"rb" => "\xBC" 
		);
	}
	
	at ( $x, $y, "{$border['lt']}{$border['t']}{$border['rt']}" );
	for($z = 1; $z < $h; $z ++) {
		at ( $x + $z, $y, $border ["l"] . $border ["bg"] . $border ["r"] );
	}
	at ( $x + $h, $y, $border ["lb"] . $border ["b"] . $border ["rb"] );
}
function box($x, $y, $w, $h, $caption) {
	border ( $x, $y, $w, $h, " ", true );
	
// 	at ( $x, $y + 3, "\xB4" );
// 	at ( $x, $y + 4, " $caption ", color ( "inverse" ) );
// 	at ( $x, $y + 6 + strlen ( $caption ), "\xC3" );
	
	at ( $x, $y + 1, "\xB4" );
	at ( $x, $y + 2, " $caption ", color ( "inverse" ) );
	at ( $x, $y + 3 + strlen ( $caption ), "\xC3" );
	
}
function screen($module) {
	cls (); // limpa a tela
	
	border ( SCREEN_X, SCREEN_Y, SCREEN_W, SCREEN_H, BG25 ); // borda externa
	
	at ( SCREEN_X, SCREEN_Y + SCREEN_W - 11, "\xCB" );
	at ( SCREEN_X + 1, SCREEN_Y + 2, sprintf ( "%-" . (SCREEN_W - 12) . "s%8s", $module, date ( "d/m/Y" ) ), color ( "green" ) );
	at ( SCREEN_X + 2, SCREEN_Y, "\xCC" . str_repeat ( "\xC4", SCREEN_W - 2 ) . "\xB9" ); // separador do rodape
	at ( SCREEN_X + 1, SCREEN_Y + SCREEN_W - 11, "\xB3" );
	at ( SCREEN_X + 2, SCREEN_Y + SCREEN_W - 11, "\xC1" );
	at ( SCREEN_X + SCREEN_H - 2, SCREEN_Y, "\xCC" . str_repeat ( "\xC4", SCREEN_W - 2 ) . "\xB9" ); // separador do rodape
	
	message ( "" );
}
function message($text, $error = false) {
	$color = $error ? "red" : "off"; // operador tern�rio
	
	at ( SCREEN_X + SCREEN_H - 1, SCREEN_Y + 2, sprintf ( "%-" . (SCREEN_W - 2) . "s", "Mensagem:" ) );
	at ( SCREEN_X + SCREEN_H - 1, SCREEN_Y + 12, sprintf ( "%-" . (SCREEN_W - 12) . "s", $text ), color ( $color ) );
}

function menu($caption, $options) {
	$largOption = 20;
	$x = round ( (SCREEN_H - count ( $options ) - 6) / 2 );
	$y = round ( (SCREEN_W - $largOption - 2) / 2 ); 
	$w = $largOption; // largura da op��o (16) mais as bordas
	$h = count ( $options ) + 2; // numero de op��es mais as bordas e sair
	
	box ( $x, $y, $w, $h, $caption );
	
	$menu = array ();
	$n = 0;
	foreach ( $options as $option ) {
		$n ++;
		$menu [] = sprintf ( "%1.1d-%-16s", $n, $option );
	}
	$menu [] = sprintf ( "%1.1d-%-16s", 0, "Voltar" );
	
	$n = 0;
	foreach ( $menu as $option ) {
		$n ++;
		at ( $x + $n, $y + 1, $option );
	}
	
	message ( "Selecione uma op��o e <ENTER> para executar" );
	$selection = null;
	while ( $selection == null ) {
		$op = fgets(STDIN);
		if ($op < "0") {
			message ( " Selecione uma op��o e <ENTER> para executar" );
		} else if ($op > count($options)) {
			message ( "Op��o inv�lida.", true );
		} else {
			at ( $x + $op, $y + 1, $menu[$op-1], color("inverse") );
			$selection = $op;
		}
	}
	
	return intval($selection);
}
