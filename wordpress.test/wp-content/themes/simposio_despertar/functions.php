<?php 

global $wp;

$current_url = home_url(add_query_arg(array(),$wp->request));

function sd_stylus() {
	wp_enqueue_style('style', get_template_directory_uri().'/style.css', array(), '1.0');
	wp_enqueue_style('boostrap-min', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_script('bootstrap-js-min', get_template_directory_uri().'/js/boostrap.min.js');
	wp_enqueue_script('jquery', get_template_directory_uri().'/js/jquery.js');
	wp_enqueue_script('jquery-3.2', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
	wp_enqueue_script('bootstrap-js-min-3.2', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
	wp_enqueue_style('fonts', get_template_directory_uri().'/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('font-source', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic');
	
}

add_action('wp_enqueue_scripts', 'sd_stylus');
