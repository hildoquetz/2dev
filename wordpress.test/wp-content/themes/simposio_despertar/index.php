<?php get_header(); ?>

    <!-- menu -->
	<nav class="navbar navbar-inverse navbar-fixed-top" style="margin: 0px; border-radius: 0px; background-color: white;">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" style="background-color: #A9AED8;" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar" ></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	      <!-- <a class="navbar-brand" href="<?php // echo site_url(); ?>"><img class="site-logo" src="<?php // echo get_template_directory_uri().'/img/partners/logoetica.jpg'; ?>"></a> -->
	    </div> 
	    <div class="collapse navbar-collapse" id="myNavbar">
	    	<ul class="nav navbar-nav navbar-right justify-content-center text-center" style="margin-bottom: 0px; margin-top: 0px;">
				<li><a href="http://www.eticacongressos.com.br/produto/simposio-internacional-despertar-da-consciencia-ascencao-e-evolucao/" target="_blank" style="background-color:#43a047; color: white;">INSCREVA-SE</a></li>
	      	</ul>
	      <ul class="nav navbar-nav navbar-right">
	      	<li class="menu-item"><a href="#o-simposio">O Simpósio</a></li>
	        <li class="menu-item"><a href="#palestrantes">Palestrantes</a></li>
	        <li class="menu-item"><a href="#local">Local</a></li>
	      </ul>	      
	    </div>
	  </div>
	</nav>
    
    <!-- Vídeo -->
    <header id="top" class="header container-fluid text-vertical-center main-bg" style="padding:0px;">
    	<div id="mobile-hidden">
        <video autoplay loop poster="http://quetz.com.br/wp-content/uploads/2017/07/main-bg.png" muted="muted">
            <source id="mp4" src="<?php echo get_template_directory_uri().'/video/bg.mp4'?>" type="video/mp4">
            <source id="ogg" src="<?php echo get_template_directory_uri().'/video/bg.ogg'?>" type="video/ogg">
            Seu navegador não suporta este recurso, <a href="http://www.updateyourbrowser.net/pt" target="_blank">clique aqui</a> para atualizar.
        </video>
        </div>
        <div class="text-vertical-center">
        	<img id="header-logo" src="<?php echo get_template_directory_uri().'/img/simposio-logo.jpg'; ?>" />
            <hr class="text-center" style="width:50%;" />
            	<h3 style="text-shadow: 2px 2px black; color: white;"><strong>Dias 7, 8 e 9 de Outubro 2017 - Espaço Immensità</strong></h3>
            <hr class="text-center" style="width:50%;" />
            <div class="container">
            	<h3 id="head-sub" style="text-shadow: 1px 1px black; color: white;">Os maiores missionários na propagação da luz e da evolução no planeta reunidos em um único evento, compartilhando conhecimento e semeando a paz interior.</h3> 
            </div>
            <div class="container-fluid">
                <div id="mobile-hidden" class="col-md-12">
                    <div class="row-fluid justify-content-center text-center">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <img class="person-star" src="<?php echo get_template_directory_uri().'/img/person/monjacoen.jpg'; ?>"> 
                            <h4><strong>Monja Coen</strong></h4>
<!--                             <h5><strong>Título da palestra</strong></h5> -->
                        </div>
                        <div class="col-md-2">
                            <img class="person-star" src="<?php echo get_template_directory_uri().'/img/person/rene.jpg'; ?>"> 
                            <h4><strong>René Mey</strong></h4>
<!--                             <h5><strong>Título da palestra</strong></h5> -->
                        </div>
                        <div class="col-md-2">
                            <img class="person-star" src="<?php echo get_template_directory_uri().'/img/person/tom.jpg'; ?>"> 
                            <h4><strong>Tom Campbell</strong></h4>
<!--                             <h5><strong>Título da palestra</strong></h5> -->
                        </div>
                        <div class="col-md-2">
                            <img class="person-star" src="<?php echo get_template_directory_uri().'/img/person/emir.jpg'; ?>"> 
                            <h4><strong>Emir Salazar</strong></h4>
<!--                             <h5><strong>Título da palestra</strong></h5> -->
                        </div>
                        <div class="col-md-2">
                            <img class="person-star" src="<?php echo get_template_directory_uri().'/img/person/maura.jpg'; ?>"> 
                            <h4><strong>Maura de Albanesi</strong></h4>
<!--                             <h5><strong>Título da palestra</strong></h5> -->
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="text-center" style="margin-top: 250px;">
            			<h4 id="em-breve">EM BREVE MAIS PALESTRANTES CONFIRMADOS</h4>
					</div>
                </div>
            </div>

        </div>
    </header><!-- video-container -->
    
    <!-- CTA 1 -->
    
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Inscreva-se agora!</h2>
                    <h4>Clique aqui e inscreva-se agora para garantir o valor promocional.</h4>
                    <br/>
                    <a href="http://www.eticacongressos.com.br/produto/simposio-internacional-despertar-da-consciencia-ascencao-e-evolucao/" target="_blank"><buttom type="buttom" class="btn btn-success btn-lg">SIM, EU QUERO EVOLUIR!</buttom></a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- O Simpósio -->
    <section id="o-simposio" class="soft-bg">
		<div class="container-fluid" style="padding: 0px">
        	<div class="col-md-12 col-sm-12">
        		<div class="row">
	        		<div class="space"></div>
	        			<h2 class="text-center"><strong>O Simpósio</strong></h2>
	        		<div class="space"></div>
        		</div>
        		<div class="row">
	        		<div class="col-lg-1 col-md-1 col-sm-12"></div>
	        		<div class="col-lg-6 col-md-6 col-sm-12">
                    	<span style="margin-top:auto;margin-bottom:auto;"><h4>O Simpósio Internacional Despertar da Consciência, Ascensão e Evolução reúne grandes mentes das áreas da espiritualidade, ciência, filosofia, artes e desenvolvimento humano em um único evento. Esses verdadeiros mestres, muitos deles em sua primeira visita ao Brasil, têm todos o mesmo objetivo: propagar a luz e impulsionar a evolução humana a nível planetário. Em três dias de evento, serão 15 palestrantes de renome nacional e internacional abordando temas fundamentais para o autoconhecimento, a auto cura e o autodesenvolvimento. São esperadas mais de mil pessoas de todo o mundo, entre livres pensadores, terapeutas holísticos, cientistas, coachs, psicólogos, médicos e espiritualistas. Venha fazer parte você também! Durante o Simpósio ainda serão oferecidos cursos e workshops profissionalizantes que podem mudar a sua carreira!</h4></span>
	        			<div class="space"></div>
	        		</div>
	        		<div class="col-lg-4 col-md-4 col-sm-12">
	        			<div class="video-responsive">
               				<iframe width="720" height="480" src="https://www.youtube.com/embed/xD7Yb6pxHO4" frameborder="0" allowfullscreen></iframe>
            			</div>
	        		</div>
	        		<div class="col-lg-1 col-md-1 col-sm-12"></div>
	        		<div class="col-md-12 col-sm-12 text-center">
	        			<div class="space"></div>
	        			<a href="http://www.eticacongressos.com.br/produto/simposio-internacional-despertar-da-consciencia-ascencao-e-evolucao/" target="_blank"><buttom class="btn btn-success btn-lg">ADQUIRIR INGRESSO</buttom></a>
	        			<div class="space"></div>
	        			<div class="space"></div>
	        		</div>
        		</div>
        	</div>
			
            </div>
        </div>
    </section>

    <!-- Local -->
    <section id="local">
    	<div class="container-fluid no-padding">
    		<div class="col-lg-12 col-md-12 col-sm-12 no-padding">
    			<div class="row no-margin">
    				<div class="col-lg-5 col-md-5 col-sm-12 no-padding">
    					<div style="padding:25px;">
							<h2 class="text-center local-item">Detalhes do Evento</h2>
							<h3 class="local-item"><i class="fa fa-calendar fa-1x"></i> Data</h3></li>
							<h4>7, 8 e 9 de Outubro de 2017</h4></li>
							<h3 class="local-item"><i class="fa fa-map-marker fa-1x"></i> Local</h3></li>
							<h4>Av. Luiz Dumont Villares, 392 - Santana, São Paulo</h4></li>
						</div>		
    				</div>
    				<div class="col-lg-7 col-md-7 col-sm-12 map no-padding">
    					<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.941770071884!2d-46.61478588552982!3d-23.498606784714056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef621e26df61d%3A0xef0448bf530d202d!2zRXNwYcOnbyBJbW1lbnNpdMOg!5e0!3m2!1sen!2sbr!4v1499859990052" allowfullscreen></iframe>
        				<br />
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <!-- CTA 2 -->
    
    <aside class="call-to-action main-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="col-sm-8 wh">
                        <h4>Sim, quero fazer parte do movimento mundial para Despertar da Consciência, Ascensão e Evolução! <a href="http://www.eticacongressos.com.br/produto/simposio-internacional-despertar-da-consciencia-ascencao-e-evolucao/" target="_blank" style="text-decoration:none; color: #6a3093;"><strong>Clique aqui</strong></a> e inscreva-se agora para garantir o valor promocional.</h4>
                    </div>
                    <div class="col-sm-4">
                        <a href="http://www.eticacongressos.com.br/produto/simposio-internacional-despertar-da-consciencia-ascencao-e-evolucao/" target="_blank" class="btn btn-lg btn-light">Garantir ingresso agora</a>
                    </div>
                </div>
            </div>
        </div>      
    </aside>
    
    <!-- Palestrantes -->
    
    <section id="palestrantes">
        <div class="container">
            <div class="col-sm-12 text-center">
                <div class="space"></div>
                <h2 style="margin-top: 8px;"><strong>Palestrantes</strong></h2>
                <div class="space"></div>
            </div>
            <div class="row text-center">
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/dibiasi.jpg'; ?>"> 
                    <h3><strong>Francisco Di Biase</strong></h3>
                    <h4><strong>Neurocirurgião</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/monjacoen.jpg'; ?>"> 
                    <h3><strong>Monja Coen</strong></h3>
                    <h4><strong>Líder Budista</strong></h4>
                </div>                
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/emir.jpg'; ?>"> 
                    <h3><strong>Emir Salazar</strong></h3>
                    <h4><strong>Líder espiritual idealizador do projeto arco-íris</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/ernesto.jpg'; ?>"> 
                    <h3><strong>Ernesto Gonzales</strong></h3>
                    <h4><strong>Engenheiro - Biodecodificação e Desdobramento do Tempo</strong></h4>
                </div>
                
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/maria.jpg'; ?>"> 
                    <h3><strong>Ana Maria Oliva</strong></h3>
                    <h4><strong>Biomédica - Doutora em Biomedicina</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/maura.jpg'; ?>"> 
                    <h3><strong>Maura de Albanesi</strong></h3>
                    <h4><strong>Psicóloga e escritora</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/monica.jpg'; ?>"> 
                    <h3><strong>Mônica de Medeiros</strong></h3>
                    <h4><strong>Médica cirurgiã</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/natalia.jpg'; ?>"> 
                    <h3><strong>Natália Fesik</strong></h3>
                    <h4><strong>Filósofa e instrutora do curso de Regeneração Celular</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/silvana.jpg'; ?>"> 
                    <h3><strong>Silvana da Cunha</strong></h3>
                    <h4><strong>Terapeuta Holistica e Fundadora TQC - Cura Quantica &reg;</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/rosangela.jpg'; ?>"> 
                    <h3><strong>Rosângela Arnt</strong></h3>
                    <h4><strong>Médica Quântica</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/tom.jpg'; ?>"> 
                    <h3><strong>Tom Campbell</strong></h3>
                    <h4><strong>Físico NASA</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/simone.jpg'; ?>"> 
                    <h3><strong>Simone Arrojo</strong></h3>
                    <h4><strong>Comunicadora e Terapeuta em Constelação Familiar Sistêmica</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person" src="<?php echo get_template_directory_uri().'/img/person/rene.jpg'; ?>"> 
                    <h3><strong>René Mey</strong></h3>
                    <h4><strong>Humanitário e Curador</strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img class="person persob-adj" src="<?php echo get_template_directory_uri().'/img/person/jorge.jpg'; ?>"> 
                    <h3><strong>Jorge Sato</strong></h3>
                    <h4><strong><hr class="small"/></strong></h4>
                </div>
                <div class="col-md-3 col-sm-4 person-adj">
                    <img style="display: inline; position: relative; display: inline;" class="person person-adj" src="<?php echo get_template_directory_uri().'/img/person/malu.jpg'; ?>">
<!--                    	<div style="display: inline; position: relative; display: inline;">
                    		<img style="display: inline; position: relative; display: inline;" class="person person-adj" src="<?php echo get_template_directory_uri().'/img/person/malu.jpg'; ?>">
                     	</div> -->
                    <h3><strong>Karla, Malu e Rogério</strong></h3>
                    <h4><strong>Terapeutas holísticos e embaixadores da The Reconnection no Brasil</strong></h4>
                </div>
<!--                 <div class="col-md-3 col-sm-4 person-adj"> -->
<!--                <img class="person person-adj" src="<?php // echo get_template_directory_uri().'/img/person/nataia.jpg'; ?>"> --> 
<!--                     <h3><strong>Margarete Áquila</strong></h3> -->
<!--                     <h4><strong><hr class="small"/></strong></h4> -->
<!--                 </div> -->
                
            </div>          
        </div>
        <div class="space"></div>
    </section>
    
    <!-- Patrocinadores -->
    
    <section id="patrocinio" class="soft-bg">
        <div class="container-fluid">
            <div class="space"></div>
            <div class="col-12-md text-center">
                <div class="row">
                    <h2><strong>Realização</strong></h2>
                </div>
                <div class="space"></div>
                <div class="row partners">
                    <img src="<?php echo get_template_directory_uri().'/img/partners/logoetica.jpg'; ?>">
                	<div class="space"></div>
                	<div class="space"></div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Social -->
    
    <section id="social" class="main-bg">
        <div class="container-fluid">
            <div class="space"></div>
            <h2 class="wh text-center">Compartilhe esta ideia :)</h2>
            <div class="space"></div>
            <div class="col-sm-12 text-center">
                <a href="https://www.fb.com/simposiodespertar" target="_blank"><i class="fa fa-facebook-square fa-4x wh sm"></i></span></a> 
                <a href="https://www.instagram.com/simposiodespertar/" target="_blank"><i class="fa fa-instagram fa-4x wh sm"></i></a>
                <a href="https://www.youtube.com/user/CONAMERICAEVENTOS" target="_blank"><i class="fa fa-youtube-square fa-4x wh sm"></i></a>
            <div class="space"></div>
            </div>
        </div>
    </section>
    
<?php get_footer(); ?>
