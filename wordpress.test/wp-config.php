<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_test_enviroment');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '130293');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N`H!tIesaPm^yPV}9SVPB%~w(&C2A=T&3?-]N)`YNJ=pKYgup594|QCY-{~9Sm}u');
define('SECURE_AUTH_KEY',  'uN(!71[ZVpn>C0gJ@65od`X3vGLN>AB!_PE$}(H08#)$_i*3]IRM73Y6p`GGz3K@');
define('LOGGED_IN_KEY',    '*%ao75,#bW=^}Ngb*o<?&l/W%({L#M4=1YjC_T8~5OKZTaX.`HOQP,05k^LXy%O5');
define('NONCE_KEY',        'xZk~Jlj6`}5XkSSu$A;7&Yl%Gb8TZ0kgvK>hw;gC<iS+v=$$yl+NXAV?N@~cl%-M');
define('AUTH_SALT',        '/I qV+UOxWfa)C*4T~~f?`3f5eX5$Tp.8/OmFt,ii(AHR62plF|s=HV|yId=4 Yp');
define('SECURE_AUTH_SALT', '/DF2opuTm6E];YI)82 E)y]$ndmt?a9bDT{]^vZ]f&[ST=YG!F[@W^>HG0W?>a9%');
define('LOGGED_IN_SALT',   'Q^h}`3?K} m|xj#Hwh,TPj5]6DA0504q{NnmDEA)`o}}e`#[Xm%fJP3hqK4;9L!R');
define('NONCE_SALT',       ';r:8}5l*rWs+_@X]>x}eeo=f1{MWf$Ft@j@%5bMN<;,2Z:W_n8cf0Ef`CXPtun9b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
