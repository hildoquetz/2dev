<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Curso PHP</title>
</head>
<body>
	<div class="container">
	<h1>CEV - Curso de PHP para Iniciantes</h1>
	<a href="https://www.youtube.com/watch?v=1KdhIz0Gh5A" target="_blank"><button>Retornar de onde parou</button></a>

	&nbsp;
	<h3>Índice</h3>
	<ul style="list-style: none;">
		<li style="font-size: 16px;"><a href="http://localhost/www/phpdev/index.php#4">#04 - Variáveis no PHP</a></li>
		<li style="font-size: 16px;"><a href="http://localhost/www/phpdev/index.php#5">#05 Operadores Aritiméticos</a></li>
		<li style="font-size: 16px;"><a href="http://localhost/www/phpdev/index.php#6">#06 Operadores de Atribuição</a></li>
		<li style="font-size: 16px;"><a href="http://localhost/www/phpdev/index.php#7">#07 Operadores Relacionais</a></li>
	</ul>
	&nbsp;
	<hr />
	<section id="4"></section>
	<h1>#04 - Variáveis no PHP</h1>
	<ul>
		<li>Não é preciso setar as variáveis, elas mudam automaticamente de acordo com o tipo de dado inserido;</li>
		<li>É possível forçar as variáveis (<strong>typecast</strong>) da seguinte forma:</li>
		<ul>
			<li><b>Inteiro:</b> (int), (integer);</li>
			<li><b>Real:</b> (real), (float), (double);</li>
			<li><b>Caractere:</b> (string);</li>
			<li><b>Lógico:</b> 1 = true / vazio = false;</li>
		</ul>
		<li>Os nomes de variáveis são <em>case sensitive</em>;</li>

	</ul>
	<h2>Exemplos:</h2>

	<?php 
	
	$n = 2017-1993;
	$nome = "Hildo Quetz";
	echo $nome." tem ".$n." anos. <br />";
	echo "$nome tem $n anos.";

	?>

	<hr />
	<section id="5"></section>
	<h1>#05 Operadores Aritiméticos</h1>

	<ul>
		<li>Adição = +</li>
		<li>Subtração = -</li>
		<li>Multiplicação = *</li>
		<li>Divisão = /</li>
		<li>Módulo = %</li>
	</ul>

	<h3>Exemplo</h3>
	<p>
	$n1 = 2; <br />
	$n2 = 3; <br />
	</p>

	<?php 
	$n1 = 2;
	$n2 = 3;
	echo "A soma vale ".($n1+$n2);
	echo "<br />A subtração vale ".($n1-$n2);
	echo "<br />A Multiplicação vale ".($n1*$n2);
	echo "<br />A Divisão vale ".($n1/$n2);
	echo "<br />O Módulo vale ".($n1%$n2);
	?>

	<h3>Ordem de execução das operaçẽos</h3>
	<ul>
		<li>#1 Parênteses = ();</li>
		<li>Multipliplicação / Divisão / Módulo = * / %;</li>
		<li>Adição / Subtração = + -;</li>
	</ul>

	<h3>Exemplo</h3>
	<?php 

	$media = ($n1 + $n2) / 2;
	echo "Média entre N1 e N2 = $media";

	?>

	<h2>Usando GET para passar parâmetros</h2>

	<p>Como fica no php:</p>

	<p>$b1 = $_GET["a"]; <br />
	$b2 = $_GET["b"];</p>

	<p>Como fica a Url: http://localhost/www/phpdev/<strong>?a=3&amp;b=2</strong></p> 

	<?php

	$b1 = $_GET["a"];
	$b2 = $_GET["b"]; 
	
	echo "Teste de valores recebidos: <br />&#36;b1 = $b1 <br />&#36;b2 = $b2";

	?>

	<h2>Funções Predefinidas do PHP</h2>
	<ul>
		<li><strong>abs()</strong><br />
		Transforma em absoluto variáveis negativas.</li>
		<li><strong>pow()</strong><br />
		Potenciação.</li>
		<li><strong>sqrt()<br /></strong>
		Raiz quadrada.</li>
		<li><strong>round() / ceil / floor<br /></strong>
		Arredondamento de números reais, sendo <em>round</em> arredondando de acordo com o decimal (acima de .5 para cima), <em>floor</em> sempre para baix e <em>ceil</em> sempre para cima.</li>
		<li><strong>intval()<br /></strong>
		Captura a parte inteira de um número real</li>
		<li><strong>number_format()</strong><br />
		Formata números reais para moeda de acordo com os parâmetros.</li>
	</ul>

	<h3>Exemplos</h3>
	<h4>abs()</h4>
	<p>$x = 10;<br />
	$y = 20;<br />
	$xy = $x - $y;</p>
	
	<?php 

	$x = 10;
	$y = 20;
	$xy = $x - $y;

	echo "Sem abs = $xy <br />Com abs = ".abs($xy);

	?>

	<h4>pow()</h4>
	<p>$x = 3;<br />
	$y = 5;<br />
	pow($x, $y);</p>

	<?php 

	$x = 3;
	$y = 5;
	$xy = pow($x, $y);

	echo "O valor de $x<sup>$y</sup> = $xy";

	?>

	<h4>sqrt()</h4>
	<p>$x = 4;<br />
	$y = sqrt($x);</p>
	
	<?php 

	$x = 4;
	$y = sqrt($x);

	echo "A raiz de $x é $y.";

	?>

	<h4>round() / ceil() / floor()</h4>
	<p>$x = 3.25;<br />
	$y = 5.75;<br />
	$z = 7.5;</p>

	<?php 

	$x = 3.25;
	$y = 5.75;
	$z = 7.5;
	echo "round para z = ".round($z)."<br />floor para y = ".floor($y)."<br />ceil para x = ".ceil($x);

	?>

	<h4>intval()</h4>
	<p>$x = 3.25;</p>

	<?php 

	$x = 3.25;
	echo "O valor inteiro para x é = ".intval($x);

	?>

	<h4>number_format()</h4>
	
	<p>Sitaxe:
	<br />number_format($var, x, y, z);</p>

	<p><b>Onde:</b></p>
	<p><b>x</b> = número de casas decimais;<br />
	<b>y</b> = separador das casas decimais;<br />
	<b>z</b> = separado de milhar;</p>

	<h4>Exemplo</h4>

	<p>$x = 3060.25;<br />
	echo "O valor &#36;x em modeda é = ".number_format($x,2,",",".");</p>

	<?php 

	$x = 3060.25;
	echo "O valor &#36;x em modeda é = R&#36; ".number_format($x,2,",",".");

	?>

	<hr />
	<section id="6"></section>
	<h1>#6 Operadores de Atribuição</h1>

	<h3>Exemplos</h3>

	<p>
		$a = 1;<br />
		$b = 3;<br /><br />
		$c = $a + $b;<br />
		$c = $c + 5; // <b>ou</b> $c += 5;<br />
		$b = $b + $a; // <b>ou</b> $b += $a;<br />
		$a = $a + 1; // <b>ou</b> $a += 1; // <b>ou</b> $a++;
	</p>

	<h3>Exercício</h3>
	<p>Adicione 10% ao valor de um produto. Use o método GET via p.</p>
	&nbsp;
	
	<?php

		$preco = $_GET["p"];
		$preco += ($preco * 10 / 100);

		echo "O preço do produto é R&#36;".number_format($preco,2,",",".");

	?>

	<h3>Operadores de Incremento/Decremento</h3>

	<p>O <b>Pré-decremento</b> utiliza a variável antes de incrementar, e o <b>Pós-incremento</b> utiliza a variável posteriormente ao</p>

	<ul>
		<li><b>Pré-incremento</b><br />Exemplo: $a++;</li>
		<li><b>Pós-incremento</b><br />Exemplo: ++$a;</li>
		<li><b>Pré-decremento</b><br />Exemplo: $a--;</li>
		<li><b>Pós-decremento</b><br />Exemplo: --$a;</li>
	</ul>

	<h3>Comentários</h3>
	<h4>Inline</h4>
	<p style="color: gray;">// Comentário <br /> # Comentário </p>
	<h4>Multiline</h4>
	<p style="color: gray;">/*Eu sou <br />um comentário<br />de múltiplas linhas */</p>

	<h3>Referências entre variáveis</h3>
	<p>Permite fazer referências de herança ou permante entre variávies.</p>

	<h4>Exemplos:</h4>

	<p>
	<span class="c">// Referência de herança</span><br />
	$a = 3;<br />
	$b = $a; <span class="c">// $b recebe o valor de $a</span><br />
	$b += 5; <span class="c">// $b = 8 (3 de $a + 5)</span><br />
	<br />
	<span class="c">// Referência permanente</span><br />
	$b = &amp;$a; <span class="c">// todas as mudanças de $b ocorrem também em $a</span><br />
	$b += 5; <span class="c">//$b = 8 e $a = 8</span>
	</p>
	<p><b>Brincando um pouco...</b></p>
	<?php 
	$a = 6;
	$b = $a;
	$b += 4;
	echo "$b";

	$b = &$a;
	echo "<br />$b";
	$b++;
	echo "<br />b = $b e a = $a";
	$a = 20;
	echo "<br />b = $b e a = $a";
	?>

	<h3>Variáveis de Variáveis</h3>
	<b>Exemplo</b>
	<p>
		$site = "quetz";<br />
		<b>$</b>$site = "hildo";<br />
		echo $site; <span class="c">// Vai exibir 'quetz'</span><br />
		echo $quetz; <span class="c">// Vai exibir 'hildo'</span>
	</p>
<hr />
<section id="7"></section>
<h1>#07 Operadores Relacionais</h1>

	<ul>
		<li><b>Maior</b><br />$a > $b</li><br />
		<li><b>Menor</b><br />$a < $b</li><br />
		<li><b>Maior ou igual</b><br />$a >= $b</li><br />
		<li><b>Menor ou igual</b><br />$a <= $b</li><br />
		<li><b>Diferente</b><br />$a != $b <b>ou</b> $a <> $b</li><br />
		<li><b>Igual</b><br />$a == $b</li><span class="c">// se utilizar apenas '=' vc acaba atribuindo $b à $a, cuidado!</span><br />
		<li><b>Idêntico</b><br />$a === $b</li><span class="c">// testa se a variável possui o mesmo valor e tipo</span>
	</ul>

	<h4>Exempo de Idêntico</h4>
	<p>
	$a = "3"; <span class="c">// string</span><br />
	$b = 3; <span class="c">// inteiro</span><br />
	$a == $b; <span class="c">// TRUE</span><br />
	$a ===$b; <span class="c">// FALSE</span>
	</p>

	<h3>Operador Ternário</h3>
	<p>Forma prática de fazer um if-else</p>
	&nbsp;
	<b>Sintaxe:</b>
	<p>$teste = ($a > $b) ? $a+$b : $a - $b<br />
	<span class="c">// será testado se $a é maior que $b, caso positivo, $a e $b serão somados, se negativo, $b será subtraído de $a</span></p>

	<?php 
	$nnn1 = 3;
	$nnn2 = 2;

	echo "nnn1 = $nnn1<br />nnn2 = $nnn2<br />";

	$teste = ($nnn1 > $nnn2) ? $nnn1+$nnn2 : $nnn1-$nnn2;
	echo "$teste TRUE";

	$teste = ($nnn1 < $nnn2) ? $nnn1+$nnn2 : $nnn1-$nnn2;
	echo "<br />$teste FALSE";

	// Usando operador ternário no echo

	echo "<br />O resultado da operação é ".(($nnn1 > $nnn2) ? $nnn1+$nnn2 : $nnn1-$nnn2);

	?>

	<h3>Operadores Lógicos</h3>
	<div class="well">
		<h3>Operador E</h3>
		<table class="table">
			<thead>
				<tr>
					<th>p</th>
					<th>q</th>
					<th>p E q</th>
				</tr>
			</thead>
			<tr>
				<td>V</td>
				<td>V</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>V</td>
				<td>F</td>
				<td class="danger">F</td>
			</tr>
			<tr>
				<td>F</td>
				<td>V</td>
				<td class="danger">F</td>
			</tr>
			<tr>
				<td>F</td>
				<td>F</td>
				<td class="danger">F</td>
			</tr>
		</table>

		<h4>Em php, utiliza-se 'and' ou '&amp;&amp;'</h4>
	</div>
	&nbsp;
	<div class="well">
		<h3>Operador OU</h3>
		<table class="table">
			<thead>
				<tr>
					<th>p</th>
					<th>q</th>
					<th>p OU q</th>
				</tr>
			</thead>
			<tr>
				<td>V</td>
				<td>V</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>V</td>
				<td>F</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>F</td>
				<td>V</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>F</td>
				<td>F</td>
				<td class="danger">F</td>
			</tr>
		</table>

		<h4>Em php, utiliza-se 'or' ou '||' </h4>
	</div>
	&nbsp;

	<div class="well">
		<h3>Operador XOU (Ou Exlusivo)</h3>
		<table class="table">
			<thead>
				<tr>
					<th>p</th>
					<th>q</th>
					<th>p XOU q</th>
				</tr>
			</thead>
			<tr>
				<td>V</td>
				<td>V</td>
				<td class="danger">F</td>
			</tr>
			<tr>
				<td>V</td>
				<td>F</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>F</td>
				<td>V</td>
				<td class="success">V</td>
			</tr>
			<tr>
				<td>F</td>
				<td>F</td>
				<td class="danger">F</td>
			</tr>
		</table>

		<h4>Em php, utiliza-se 'xor' ou '||' </h4>
	</div>
	&nbsp;

	<div class="well">
		<h3>Operador NÃO</h3>
		<table class="table">
			<thead>
				<tr>
					<th>p</th>
					<th>p Não q</th>
				</tr>
			</thead>
			<tr>
				<td>V</td>
				<td class="danger">F</td>
			</tr>
			<tr>
				<td>F</td>
				<td class="success">V</td>
			</tr>
		</table>
		<h4>Em php, utiliza-se '!'</h4>
	</div>
	&nbsp;

	<h4>Exercíco</h4>

	<?php 

		$anoNac = 2013;
		$idade = 2017 - $anoNac;
		$tipo = ($idade >= 18 && $idade <= 65) ? "obrigatório":"não obrigatório";

		echo "Quem nasceu em $anoNac tem $idade anos. Então o voto é $tipo.";

	?>

	<h1>#08 Integração HTML PHP</h1>


	<h4>Calculando a raiz quadrada</h4>

	<form method="get" action="dados.php">
		Valor:
		<input type="text" name="v" />
		<input type="submit" value="calcular">
	</form>
	&nbsp;
	<h4>Formulário de teste</h4>
	<form method="get" action="dados.php">
		<div class="form-group">
		Nome:
		<input type="text" name="nome" /><br />
		Ano de Nascimento:
		<input type="text" name="anoN"><br />
		<fieldset>
			<legend>
				Sexo
			</legend>
			<input type="radio" name="sexo" id="mas" value="bixona" checked>
			<label for="masc">Masculino</label><br />
			<input type="radio" name="sexo" id="fem" value="baitola">
			<label for="femi">Feminino</label><br />
		</fieldset><br />
		<input type="submit" value="Enviar">
		</div>
	</form>
	&nbsp;
	<h4>Exercício Manipulando CSS com PHP</h4>
	<form method="get" action="dados.php">
		<label for="texto">Texto</label>
		<input type="text" name="texto" id="texto"><br>
		<label for="tamanho">Tamanho:</label>
		<select name="tamanho" id="tamanho">
			<option value="8pt">8</option>
			<option value="14pt">14</option>
			<option value="28pt" selected="">28</option>
			<option value="56pt">56</option>
			<option value="72pt">72</option>
		</select><br>
		<label for="cor">Cor:</label>
		<input type="color" name="cor" id="cor"><br>
		<input type="submit" value="Gerar">
	</form>

	<h1>#9 Estrutura condicional IF</h1>
	<p>Permite a execução de ações de maneira condicional. Sua vantagem frente ao operador ternário é que permite trabalhar com mais de uma variável em cada bloco true/false. </p>

	<form method="get" action="dados.php">
		<label for="ano">Informe o ano de nascimento</label><br>
		<input type="text" name="ano" id="ano"><br>
		<input type="submit" value="Verificar">
	</form>


	<h1>#10 Estrutura condicional switch</h1>

	<h3>Exercício com Switch</h3>

	<form method="get" action="dados.php">
		<label for="num">Número</label>
		<input type="text" name="num" /><br />
		<fildset>
			<legend>Operação</legend>
			<label for="dobro">Dobro</label>
			<input type="radio" name="oper" id="dobro" value="1" checked /><br />
			<label for="">Cubo</label>
			<input type="radio" name="oper" id="cubo" value="2" /><br />
			<label for="raiz">Raiz</label>
			<input type="radio" name="oper" id="raiz" value="3" /><br />
		</fildset>
		<input type="submit" value="Verificar">
	</form>


	<h1>#11 Estrutura de Repetição While</h1>

	

	<?php 

		$c = 0;

		while ($c <= 10) {
			echo "$c<br />";
			$c++;
		}


	?>

	<h3>Criando campos dinamicamente</h3>

	<form method="get" action="dados.php">
	<?php 

		$x = 1;

		while ($x <= 5) {
			echo "<label for='val$x'>Valor $x</label><input type='text' name='val$x' /><br />";
			$x++;
		}


	?>
	</form>

	<h3>Exercício contador</h3>

	<form method="get" action="dados.php" id="index">
		<label for="numIn">Num Ini</label>
		<input type="text" name="numIn" /><br />
		<label for="numFn">Num Fin</label>
		<input type="text" name="numFn" /><br />
		<label for="inde">Incremento / Decremento</label>
		<select form="index" name="ind">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="4" selected>4</option>
			<option value="6">6</option>
			<option value="8">8</option>
		</select><br />
		<input type="submit" value="Calcular" />

	</form>


	<h1>#12 Estrutura de repetição do while</h1>




	<?php 
	$c =1;
	do {
		echo " $c |";
		$c++;
	} while ($c <= 10);

	?>


	<h1>#13 Estrutura de Repetição com FOR</h1>

	<h4>Utilizamos o While quando não sabemos quantas repetições serão realizadas, já o For é quando sabemos o fim.</h4>

	<?php 

	# Estrura do for

	// for ($c=1;$c<=10;$c++) {
	// 		code...
	// }

	for ($i=1;$i<=10;$i++) {
		echo " $i |";
	}
	echo "<br>";
	for ($c=0;$c<=100;$c+=5) {
		echo "  $c |";
	}
	echo "<br/>";
	for ($d=20;$d>=0;$d-=2) {
		echo " $d |";
	}

	?>


	<h1>#14 Rotinas (funções)</h1>

	<h4>Criando uma função soma</h4>

	<?php 

	function soma ($a, $b) {
		$s = $a + $b;
		echo "O resultado da soma é $s";
	}

	soma(3,12);

	?>
	<p>Retornando valor para uma variável</p>
	<?php 
	function soma1 ($a, $b) {
		$s = $a + $b;
		return $s;
	}

	$result = soma1(2,2);

	echo "Eu quero você de $result";

	?>
	<p>Rotinas com parâmetros dinâmicos</p>

	<?php 

	function soma2 () {
		$p = func_get_args();
		$tot = func_num_args();
		$s = 0;
		for ($i=0; $i < $tot; $i++) {
			$s += $p[$i];
		}
		return $s;
	}

	$res = soma2(7, 10, 3, 15);

	echo "O resultado é $res";

	?>


	<h1>#15 Rotinas (funções) 2</h1>

	<p>Vamos verificar como retornar valores com referência.</p>

	<?php 

	include "functions.php";

	$textX = 10;

	xmall($textX); 
	echo "<br><i>textX</i> vale $textX";

	?>
	<p>Passagem por Referência</p>


	<?php 

	$y = 10;

	$x = ref($y);

	echo "Y = $y <br/>return = $x";

	?>


	<h3>Diferença entre include e require</h3>

	<p>O <i>include</i> chama o arquivo, porém caso o mesmo esteja inacessível naquele momento, ele ignora e segue a execução do script. <br />Já o <i>require</i> chama o arquivo e caso não consiga acessar, ele para a execução do script.</p>

	<h3>include_once / require_once</h3>	
	<p>A adição do '_once' faz com que o php verifique se o arquivo chamado já foi incluido. Caso positivo, ele ignora a nova ação de inclusão solicitada. Caso negativo, ele inlui o arquivo.<br />Caso seja feita a chamada de inclusão dos arquivos mais de uma vez, se todas forem bem sucedidas (e a chamada não especificar '_once)', o arquivo será incluso quantas vezes forem solicitadas;</p>

	<h1>#16 Como manipular strings em PHP</h1>

	<h3>1. printf()</h3>
	<p>Exibe a informação formatada.</p>

	<h4>Exemplo:</h4>
	<?php 

		$prod = "Leite";
		$p = 4.5;

		echo "Com <i>number_format</i><br/>O $prod custa R$ ".number_format($p,2);

		printf("<br/><br/>Com <i>printf</i><br/>O %s custa R$ %.2f", $prod, $p);

	?>
	<h4>Parâmetros do printf()</h4>
	<table class="table">
		<thead>
			<tr>
				<th>Parâmetro</th>
				<th>Descrição</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>%d</td>
				<td>Valor decimal (positivo ou negativo</td>
			</tr>
			<tr>
				<td>%u</td>
				<td>Valor decimal sem sinal (apenas positivos)</td>
			</tr>
			<tr>
				<td>%f</td>
				<td>Valor real</td>
			</tr>
			<tr>
				<td>%s</td>
				<td>String</td>
			</tr>
		</tbody>
	</table>


	<h3>2. print_r</h3>
	<p>Mostra detalhes de uma variável, princilmente vetores. Muito útil para testes.<br/>Outras funções que também podem ser utilizadas com o mesmo objetivo são a <i>var_dump()</i> e <i>var_export()</i>.</p>
	<?php 

	$vetor[0] = 2;
	$vetor[1] = 7;
	$vetor[2] = 3;

	print_r($vetor);
	echo "<br/>";
	var_dump($vetor);
	echo "<br/>";
	var_export($vetor);
	echo "<br/>";

	?>

	<h3>3. Função wordwrap</h3>
	<p>Insere quebras de linhas em lextros grandes</p>
	<h4>Exemplo</h4>
	<?php 
	$t = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
	$wrap = wordwrap($t, 20, "<br/>");
	echo $wrap;
	?>

	<h3>4. strlen</h3>
	<p>Exibe o tamanho de uma string.</p>
	
	<?php 
	
		$str = "Eu sei contar.";

		echo strlen($str);

	?>

	<h3>5. trim</h3>
	<p>Remove espaços em branco do inicio e final da string. A tratativa tem que ser gravada em uma nova variável.</p>

	<?php 
		$str = "   Eu   tenho  muitos   espaços";
		echo "$str ".strlen($str)."<br /><br />";
		$novo = trim($str);
		echo "$novo ".strlen($novo);
	?>

	<p><br/>Existem mais duas variações para esta função que são a <i>ltrim</i> e <i>rtrim</i>.</p>
	
	<p><b>ltrim</b><br/>
	Remove os espaços no início da string.</p>

	<p><b>rtrim</b><br/>
	Remove os espaços no final da string.</p>

	<h3>6. str_word_count</h3>
	<p>Exibe a quantidade de palavras dentro de uma string.</p>

	<?php 

	$txt = "Olá mundo do caralho! Eu estou contando!";
	echo str_word_count($txt, 0);

	?>

	<h3>7. explode</h3>
	<p>Cria um vetor com base no parâmetro de contagem. Pode ser espaço, ponte e virgula, etc.</p>

	<?php 
		$str = "Vamos criar através dos espaços que existem.";

		$vtr = explode(" ",$str);
		print_r($vtr);

		$str = "Agora vamos; criar via; ponto e virgula distribuidos; pelo texto;";

		echo "<br/><br/>";

		$vtr1 = explode(";",$str);
		print_r($vtr1);

	?>

	<h3>8. str_split</h3>
	<p>Função semelhante a <em>explode()</em>, porém transformanda em array por caractere. Não captura espaços em branco.</p>

	<?php 

		$nome = "Hildo Quetz Silva.;!";
		// $vtr2 = str_split($nome);
		print_r(str_split($nome));

	?>

	<h3>8. implode</h3>
	<p>Funciona de maneira inversa ao explode; ou seja, ele exibe nas posições sequenciais.</p>
	<?php 

		$vtr3[0] = "Eu";
		$vtr3[1] = "vou";
		$vtr3[2] = "ser";
		$vtr3[3] = "foda";
		$vtr3[4] = "em";
		$vtr3[5] = "php";

		echo implode(" #",$vtr3);

	?>

	<p>Uma alternativa ao <em>implode</em> é a função <em>join</em>, que funciona da mesma maneira.</p>

	<h3>9. chr</h3>
	<p>Recebe o código e exibe o caracter ASCII correspondente.</p>

	<?php 

	echo chr(67).chr(99);

	?>

	<h4>Tabela ASCII (não é completa)</h4>
	<div class="panel-group">
		<div class="panel-default">
			<div class="">
	<table class="table">
		<thead>
			<tr>
				<th>Código</th>
				<th>Caractere</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				for($i=33;$i<=126;$i++) {
					echo "<tr><td>$i</td><td>".chr($i)."</td></tr>";
				}
			?>			
		</tbody>
	</table>

	<h3>9. ord</h3>
	<p>Funciona de maneira contrária a <em>chr</em>.Ela recebe o caractere e exibe o código ASCII correspondente.</p>

	<?php 
		$str ="v";
		echo ord($str)." ".chr(ord($str));

	?>

	<h3>10. strtolower / strtoupper</h3>
	<p><b>strtolower</b><br/>Substitui todas as letras minusculas por maiusculas.<br/><br/><b>strtoupper</b><br/>Comportamento inverso da primeira função.</p>

	<?php 

		$nome="Hildo Quetz Silva";

		echo strtolower($nome)."<br/>";
		echo strtoupper($nome);

	?>

	<h3>11. ucfirst / ucwords</h3>
	<p><b>ucfirst</b><br/>Transforma a primeira letra da primeira palavra em maiuscula (uc é de <b>u</b>pper<b>c</b>ase).<br/><br/><b>ucwords</b><br/>Transforma a primeira letra de todas as palavras em maiusculo.</p>
	
	<?php 

		echo "ucfirst = ".ucfirst(strtolower($nome));
		echo "<br/>ucwords = ".ucwords(strtolower($nome));
	?>

	<h3>12. strrev</h3>
	<p>Apresenta a string de trás para frente.</p>
	<?php 
		echo strrev($nome);
	?>

	<h3>13. strpos / stripos</h3>
	<p><b>strpos</b><br/>Encontra e indica o início da posição de um termpo procurado dentro de uma string. É case sensitive.<br/><br/><b>stripos</b><br/>Faz 3o mesmo que a função anterior, porém não é case sensitive.</p>
	<?php 	

		$frase = "Eu estou aprendendo PHP, eh!";

		echo "PHP está na posição ".strpos($frase, "PHP");

	?>
	<h3>14. substr_count </h3>
	<p>Conta a incidência de presença de um termo dentro de uma string.</p>
	<?php 	

		$frase = "Eu gosto do PHP porque o PHP é muito bom!";

		echo "$frase <br/>PHP foi dito ".substr_count($frase,"PHP")." vezes.";

	?>
	
	<h3>15. substr</h3>
	<p>Existem várias parametros para ela, mas basicamente ela recebe o número inicial e final para exibição da string principal.</p>
	<?php 	

		echo substr($frase, 0, 8);
	?>
	<h3>16. str_pad</h3>
	<p>Adiciona espaços(ou outro caracter) antes ou depois da frase, completando com a quantidade restante para o total informado.</p>
	<?php 	
		$str_pad = str_pad($nome, 30, "!", STR_PAD_RIGHT);
		echo "$str_pad";
	?>
	<h3>17. str_repeat</h3>
	<p>Repete a string indicada</p>

	<?php 	
		echo str_repeat("Nain! <br/>",6);
	?>

	<h3>18. str_replace / str_ireplace </h3>
	<p>Substitui um termo por outro dentro de uma string. A diferença da função <em>str_ireplace</em> é que a mesma ignora o case sensitive.</p>
	<?php 	
		echo "Atual: <em>$frase</em><br/>Nova: ".str_replace("PHP", "PPK", $frase);
	?>
	<h1>#16 - Vetores e Matrizes</h1>
	<p>Vetores, ou <em>array</em>,  uma forma de armazenar os dados em lista, para utilizá-los posteriormente.</p>
	<pre>
	<?php 	

	// Duas formas de criar vetor

	$nb[0] = 1;
	$nb[1] = 7;
	$nb[2] = 6;

	// ou...

	$na = array(1,7,6);

	// É possível adicionar uma novo valor sem indicar a posição do vetor

	$na[] = 7;

	echo print_r($na);

	?>
	</pre>

	<h4>range</h4>
	<p>O <em>range</em> permite a criação e progreção de números gerando um array.</p>

	<?php 	
		$ver = range(10, 100, 10);
		print_r($ver);
	?>
	<p>Os parâmetros são <em>range(x, y, z)</em>, onde: <br/><br/>
	<b>x</b> = indica o número inicial do range;<br/>
	<b>y</b> = indica o número final do range;<br/>
	<b>z</b> = indica o espaço para cada ponto no range;</p>
	
	<h3>foreach</h3>
	<p>Apresenta os valores de um vetor de maneira formatada</p>

	<?php 	
		foreach ($ver as $value) {
			echo "|   $value";
		}
	?>

	<h3>unset</h3>
	<p>Desaloca uma posição específica do vetor.</p>
	<pre>
	<?php 	
		echo print_r($ver);
	?>
	</pre>
	<p>Remoção da posição 3 e 9:</p>
	<pre>
	<?php
		unset($ver[3],$ver[9]);
		echo print_r($ver);
	?>
	</pre>
	<h3>Chaves associativas</h3>
	<p>Possibilidade de criação de chaves personalizadas com diferentes tipos de velor. Exemplo:</p>
	<pre>
	<?php 	
		$user = array(
			"name"=> "Hildo Quetz", 
			"country"=> "Brazil",
			"age"=> 24);
		echo print_r($user);
	?>
	</pre>
	<p><em>foreach</em> com chaves associativas:</p>
	<?php 	
		foreach($user as $chave => $valor) {
			echo "<b>".ucfirst($chave).":</b> $valor<br/>";
		}
	?>
	<h3>Trabalhando Matrizes no PHP</h3>
	<p>Tecnicamente não existe matrizes no php, mas é possível criá-las utilizando vetores de vetores. Exemplo: </p>

	<?php 			
		$matriz = array(
			array(1,2),
			array(3,4),
			array(5,6)
			);
		echo "<p>Printando (exemplo ".$matriz[1][1].") a matriz:<br/></p>";
		
	?>
	<table class="table">
		<tbody>
			<?php 	
				for ($c=0; $c <= 2; $c++) {
					echo "<tr>";
					for ($i=0; $i <= 1; $i++) { 
						echo "<td style='text-align:center;'>".$matriz[$c][$i]."</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
	<h1>#17 Vetores e Matrizes parte 2</h1>
	<p>Funções para vetores.</p>

	<h3>print_r</h3>
	<p>Exibe os valores dos vetores sem formatação. Exemplo:</p>

	<?php 	
		echo print_r($user);	
	?>
	<p>Com a tag <em>pre</em>:</p>
	<pre>
	<?php 	
		echo print_r($user);		
	?>
	</pre>
	<h3>var_dump</h3>
	<p>Possui o mesmo comportamento que a <em>print_r</em>, porém com uma apresentação de dados diferente. Exemplo:</p>
	
	<pre>
	<?php 	
		var_dump($user);		
	?>
	</pre>
	<h3>count</h3>
	<p>Apresenta a quantidade de elementos dentro de um vetor.<b>Atenção:</b> lembre-se que o <em>array</em> começa em '0'.</p>
	<?php 	
		echo count($user);
	?>
	<h3>array_push</h3>
	<p>Adiciona elementos no fim da fila de um vetor. Não permite customizar o índice.</p>
	<?php 	
		array_push($user,"São Paulo");
		print_r($user);
	?>
	<h3>array_pop</h3>
	<p>Elimina o último elemento do vetor. </p>

	<?php 	
		array_pop($user);
		print_r($user);		
	?>
	<h3>array_unshift</h3>
	<p>Adiciona elementos no início do vetor. Também não permite customizar o índice.</p>
	<?php 	
		array_unshift($user, "Sr.");
		print_r($user);
	?>
	<h3>array_shift</h3>
	<p>Elimina o primeiro elemento do vetor.</p>
	<?php 	
		array_shift($user);
		print_r($user);
	?>
	<h3>sort</h3>
	<p>Ordena os elementos (numéricos) de um vetor do menor para o maior.</p>
	<?php 	
		$ord = array(5,7,8,1,9,4,3);
		print_r($ord);
		sort($ord);
		echo "<p><br/></p>";
		print_r($ord);
	?>
	<h3>rsort</h3>
	<p>Funcionamento contrário da <em>sort</em>, ordenando do maior para o menor.</p>
	<?php 	
		rsort($ord);
		print_r($ord);	
	?>
	<h3>asort</h3>
	<p>Possui o comportamento semelhante a função <em>sort</em>, porém neste caso os índices se mantém os mesmos (ao contrário da <em>sort</em>, onde são reatribuidos).</p>
	<?php 	
		print_r($ord);	
		asort($ord);
		echo "<p><br/></p>";
		print_r($ord);		
	?>
	<h3>arsort</h3>
	<p>Possui o comportamento semelhante a função <em>asort</em>, porém neste caso a ordem é do maior para o menor.</p>
	<?php 	
		print_r($ord);	
		arsort($ord);
		echo "<p><br/></p>";
		print_r($ord);		
	?>
	<h3>ksort</h3>
	<p>Ordenação pro chaves (keys) dos vetores, do menor para o maior.</p>

	<?php
		krsort($ord);
		print_r($ord);
		ksort($ord);
		echo "<p><br/></p>";
		print_r($ord);	
	?>
	<h3>krsort</h3>
	<p>Ordenação pro chaves (keys) dos vetores, do maior para o menor.</p>

	<?php
		print_r($ord);
		krsort($ord);
		echo "<p><br/></p>";
		print_r($ord);	
	?>

	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>
	<?php
		
	?>

	</div><!-- /content -->
</body>
</html>