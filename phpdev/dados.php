<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<meta charset="utf-8">
	<title>Dados</title>
</head>
<body>
	<button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button></a>
	<?php
		$valor = isset($_GET["v"])?$_GET["v"]:0;
		$raiz = sqrt($valor);

		// form exercício

		$nome = isset($_GET["nome"])?$_GET["nome"]:"";
		$nascimento = isset($_GET["anoN"])?$_GET["anoN"]:"";
		$sexo = isset($_GET["sexo"])?$_GET["sexo"]:"";

		if(isset($raiz)) {
			echo "<p class=alert-success>A raiz de $valor é $raiz!</p>";
		} else {
			echo "<p class=alert-danger>O valor não foi informado.";
		}

		echo "$nome <br/>$nascimento<br>$sexo";

	?>

	<h3>Manipulando CSS</h3>

	<?php 
		$texto=isset($_GET["texto"])?$_GET["texto"]:"";
		$tamanho=isset($_GET["tamanho"])?$_GET["tamanho"]:"";
		$cor=isset($_GET["cor"])?$_GET["cor"]:"";

		echo "<span style=font-size:$tamanho;color:$cor>$texto</span>";
	?>

	<h3>Estrutura elseif</h3>

	<?php 

	$ano = isset($_GET["ano"])?$_GET["ano"]:"";
	$idade = 2017 - $ano;

	if ($idade < 16) {
		echo "Você tem $idade anos e não pode votar!";
	} elseif (($idade >= 16 && $idade <= 18) || $idade >= 65) {
		echo "Você tem $idade anos e seu voto é opcional!";
	} else {
		echo "Você tem $idade anos e deve votar!";
	}

	?>
	<h3>Exercício Média</h3>
	<?php 

	$n1 = isset($_GET["n1"])?$_GET["n1"]:"";
	$n2 = isset($_GET["n2"])?$_GET["n2"]:"";
	$m = ($n1 + $n2)/2;

	if ($m < 5) { 	?> 
		<p class="alert-danger">Sua média foi de <?= $m ?>, você está <b>REPROVADO</b>!</p>
	<?php }elseif ($m >=5 && $m <7) { ?>
		<p class="alert-warning">Sua média foi de <?= $m ?>, você está em <b>RECUPERAÇÃO</b>!</p>
	<?php } else { ?>
		<p class="alert-success">Sua média foi de <?= $m ?>, você está <b>APROVADO</b>!</p>
	<?php } ?>

	<h3>Exercício Switch</h3>

	<?php

		$num=isset($_GET["num"])?$_GET["num"]:"";
		$oper=isset($_GET["oper"])?$_GET["oper"]:"";

		switch ($oper) {
			case 1:
				$r = $num * 2;
				break;
			
			case 2:
				$r = $num ^ 3;
				break;

			case 3:
				$r = sqrt($num);
		}

		echo "O resultado da operação é $r!";

		/* é possível utilizar mais de um 'case' para exibir um resultado, Ex:

		switch ($op) {
	
			case 1:
			case 2:
				$x = 10;
				break;

			case 3:
				$x = 1;
				break;

			case 4:
			case 5:
			case 6:
				$x = 20;

		}


		*/

	?>
	<h3>Exercício Contador</h3>

	<?php 

		$numIn=isset($_GET["numIn"])?$_GET["numIn"]:"";
		$numFn=isset($_GET["numFn"])?$_GET["numFn"]:"";
		$ind=isset($_GET["ind"])?$_GET["ind"]:"";

		if ($numIn < $numFn) {
			while ($numIn <= $numFn) {
				echo " $numIn |";
				$numIn += $ind;
			}

		} else {
			while ($numIn >= $numFn) {
				echo " $numIn |";
				$numIn -= $ind;
			}
		}

	?>



</body>
</html>

